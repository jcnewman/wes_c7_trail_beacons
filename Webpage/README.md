If hosting on NGINX server, copy all files to the /var/www/html folder.

Otherwise, copy all the files to the same directory and open in web browser (Firefox is preferred but others _should_ work...)

Note: There is a default file for the map showing example data around UCSD and, unless connected to an active system, it will not update