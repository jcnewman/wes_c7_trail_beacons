import serial, time, json, sys

# the port is hardware dependant.
port = serial.Serial("/dev/ttyACM0", baudrate=115200, timeout=3.0)

def readlineCR(port):
    rv = ""
    while True:
        ch = port.read()
        rv += ch
        if ch=='\r' or ch=='':
            return rv

def dd_to_dms(dd):
    mnt,sec = divmod(dd*3600,60)
    deg,mnt = divmod(mnt,60)
    return deg,mnt,sec

def dms_to_dd_lat(dms):
    dms_str = str(dms)
    d = dms_str[0:2].strip()
    m = dms_str[3:5].strip()
    s = dms_str[5:9].strip()
    
    d = int(d)
    m = int(m)
    s = int(s)
    dd = d+float(m)/60 + float(s)/360000
    return dd

def dms_to_dd_lon(dms):
    dms_str = str(dms)
    d = dms_str[0:4].strip()
    m = dms_str[5:7].strip()
    s = dms_str[7:11].strip() 
   
    d = int(d)
    m = int(m)
    s = int(s)
    dd = d-float(m)/60 - float(s)/360000
    return dd

### micro is passing us ddm, leaflet wants dd
def ddm_to_dd_lat(dms):
    dms_str = str(dms)
    d_str = dms_str[0:2].strip()
    dm_str = dms_str[2:9].strip()
    if d_str[0:1] != "0":
        d = float(d_str)
    if dm_str[0:1] !="0":
        dm = float(dm_str)*100
    
        dd = d+dm/60
    
        return dd

def ddm_to_dd_lon(dms):
    dms_str = str(dms)
    d_str = dms_str[0:4].strip()
    dm_str = dms_str[4:11].strip()

    if d_str[0:1] !=0:
        d = float(d_str)
    if dm_str[0:1] != "0":
        dm = float(dm_str)*100   
    
        dd = float(d)-float(dm)/60
        return dd
    else: 
        print("Host GPS is warming up!")
    

### read in location line from micro response and return ddm XX.YYYYYY, where XX = d and YYYYYY = dm
def read_location_serial():  ### read in and parse GPS
    GPS_str = readlineCR(port)
    lat1 = GPS_str[7:10].strip()
    lat2 = GPS_str[10:17].strip()
    lat = lat1+lat2
    if lat[0:1] != "0":
        lat = float(lat)/100  ## shift decimal place
    
    lon1 = GPS_str[19:23].strip()
    lon2 = GPS_str[23:31].strip()
    lon = lon1+lon2
    
    if lon[0:1] != "0":
        lon = float(lon)/100  ## shift decimal place
    return[lat, lon] 

def get_location():
    location = read_location_serial()
    lat_dms = location[0]
    latitude = ddm_to_dd_lat(lat_dms)
    lon_dms = location[1]
    longitude = ddm_to_dd_lon(lon_dms)
    return[latitude, longitude]

    '''
    -Node Info- 
    Node Addr: 6
    This node has been initialized.
    Freq:   915.0000000000
    Time:   5/1/19  2:57:31
    Loc:    [32 47.8170, -117 14.8496]
    Status: 0
    '''

def node_info():
    port.write("i")
    header = readlineCR(port)
    node_info_list = []
    ### read in node line
    id_str = readlineCR(port)
    id_words = id_str.split()
    node_id = id_words[2]
    node_id = int(node_id)
    ### read in initialization state
    init = readlineCR(port)  
    ### read in frequency line
    frequency_str = readlineCR(port)  
    ### read in time
    time_str = readlineCR(port) 

    ### read in location
    location = get_location()
    latitude = location[0]
    longitude = location[1] 

    if latitude != None:   
        latitude = round(latitude,7)
    if longitude != None:
        longitude = round(longitude,7)

    # read in extra line
    extra_line = readlineCR(port)
    
    ### read in and parse status
    status_str = readlineCR(port)
    
    status = status_str[8:].strip()
    status = 0

    node_info_list.append(node_id)
    node_info_list.append(status)
    node_info_list.append(longitude)
    node_info_list.append(latitude)
    print(node_info_list[0])
    print(node_info_list[3])
    print(node_info_list[2])
    return node_info_list
'''
    with open("/var/www/html/json/node.json", "w") as write_file:
        json.dump([{"path": [], "id": node_str, "status": status, "lng": longitude, "lat": latitude}], write_file)
'''

node = node_info()

