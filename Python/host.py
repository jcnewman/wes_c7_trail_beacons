import serial, time, json, sys
import node_info as info
import read_status as status

open("/var/www/html/json/node.json", "w").close()
port = serial.Serial("/dev/ttyACM0", baudrate=115200, timeout=3.0)
### read in location line from micro response and return ddm XXYYYYYY, where XX = d and YYYYYY = dm

###################################################################################################
################ START OF HOST PROGRAM ###########################################################

class Node:
    def __init__(self, node_id, lat, lon, status, path, nf_count):
        self.id = node_id 
        self.lat = lat
        self.lon = lon
        self.status = status
        self.path = path
        self.nf_count = nf_count

num_nodes = 9

node_list = [None]*(num_nodes)
host_node_info = info.node_info()

node_list[0] = Node(host_node_info[0], host_node_info[3], host_node_info[2], host_node_info[1], [],0)
timeout = 0
found_flag = 0
not_found_flag = 0

while True:
    json_list = []
    #json_list.append({"path": [], "id": node_info[0], "status": node_info[1], "lng": node_info[2], "lat": node_info[3]})
    
    node_of_interest = 2
    while node_of_interest <= num_nodes:
        node_of_interest_str = str(node_of_interest)
        port.write("s")
        port.write(node_of_interest_str)
        line = 1   
        timeout = 0
        found_flag = 0
        not_found_flag = 0 
        print("requesting node "+node_of_interest_str+" status")
   
        while line and not found_flag and not not_found_flag and timeout <= 5:
            line = port.readline()
            #print(line)
            if line:
             
                if line[0] == "N" and line[5] == node_of_interest_str:
                    print("can't find node " + node_of_interest_str)
                    not_found_flag = 1
                    if node_list[node_of_interest-1]:
                        node_list[node_of_interest-1].nf_count += 1
                        print(node_list[node_of_interest-1].nf_count)

                #if line.find('msg_type:'):
                if line[0] == "m" and line[1] == "s":
                    words = line.split()
                    if words[0] == "msg_type:" and words[1] == "ACK_STATUS_NODE_FOUND":
                        print("found node "+ node_of_interest_str)
                        found_flag =1
 
                        node_object = status.read_status(node_of_interest)
              
                        if node_object:
                            if node_list[node_object.id-1]:
                                delta_lat = abs(node_object.lat - node_list[node_object.id-1].lat)
                                print('delta lat', delta_lat)
                                if delta_lat > 0.03:
                                    node_list[node_object.id-1].id = node_object.id
                                    node_list[node_object.id-1].status = node_object.status
                                    node_list[node_object.id-1].path = node_object.path
                                    node_list[node_object.id-1].nf_count = node_object.nf_count
                                    print('HIDING MEXICO BUG')
                                    
                                else: node_list[node_object.id-1] = node_object
                                
                            else: node_list[node_object.id-1] = node_object
                                
                        node_object = None                         
            else:
                line = 1
               
                timeout += 1
                print("waiting for node "+ node_of_interest_str+ " status")
                if timeout == 2:
                    not_found_flag = 1
                    print("timeout reached, node "+node_of_interest_str+ " not found")
                    if node_list[node_of_interest-1]:
                        node_list[node_of_interest-1].nf_count += 1
                        print(node_list[node_of_interest-1].nf_count) 
                        if node_list[node_of_interest-1].nf_count >= 5:
                            node_list[node_of_interest-1].status = 0
                            node_list[node_of_interest-1].path = []
        
        for node in node_list:
            if node:
            #nodes_on_network[node.id-1]
                json_list.append({"path": node.path, "id": node.id, "status": node.status, "lng": node.lon, "lat": node.lat, "path": node.path})

        open("/var/www/html/json/node.json", "w").close()

        with open("/var/www/html/json/node.json", "wb") as write_file:
            json.dump(json_list, write_file)
        json_list = []
        print('JSON dumped')
        node_of_interest += 1       
    

    
 

    index = 0
    print("printing node list")
    for node in node_list:
        if node:
            #nodes_on_network[node.id-1] = node.id
            print("node on the list")
            print node.id
            print("\n")

        if node == None: print(index+1)  
        index +=1
    print("done printing node list")

port.close()
sys.exit()

### Status Message Response from if Node not found:
'''
Node 3 is not in the network
'''

