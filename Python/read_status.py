import serial, time, json, sys

# the port is hardware dependant.
port = serial.Serial("/dev/ttyACM0", baudrate=115200, timeout=3.0)

class Node:
    def __init__(self, node_id,lat, lon,status, path, nf_count):
        self.id = node_id 
        self.lat = lat
        self.lon = lon
        self.path = path
        self.status = status
        self.nf_count = nf_count

def read_status(node_of_interest):
    found_flag = 1

    source_node_line = port.readline()
    tgt_node_line = port.readline()
    payload_size_line = port.readline()
    payload_line = port.readline()
    noi_line = port.readline()  ### node of interest line
    noi_words = noi_line.split()
    noi_id_str = noi_words[1]
    noi_id = int(noi_id_str)
                     
    status_line = port.readline()
    status_words = status_line.split()
    status_str = status_words[1]
    status = int(status_str)
                    
    lat_line = port.readline()
    lon_line = port.readline()
    lat_words = lat_line.split()
    lon_words = lon_line.split()
    
    num_on_path_line = port.readline()
    
    node_path_line = port.readline()
    node_path_split = node_path_line.split()
    node_path_split = node_path_split[1:]
    node_path = map(int, node_path_split)
    
    lat_ddm_str = lat_words[1]
    lon_ddm_str = lon_words[1]  
    lat_deg = lat_ddm_str[0:2]
    lon_deg = lon_ddm_str[0:4]
    lat_dec_min = lat_ddm_str[2:]
    lon_dec_min = lon_ddm_str[4:]
    lat_ddm_str = lat_deg + "." + lat_dec_min
    lon_ddm_str = lon_deg + "." + lon_dec_min
    first_digit_lat = lat_ddm_str[0:1]   ### grab first digit to see if gps is warmed up                                    
    if first_digit_lat != "3":
        print("GPS warming up!")
        node_object = 0
    else:
        lat_deg = float(lat_ddm_str[0:2])
        lat_dec_min = float(lat_ddm_str[2:])*100
        latitude = lat_deg+lat_dec_min/60
        latitude = round(latitude,7)
        print(latitude)
         
        lon_deg = float(lon_ddm_str[0:4])     
        lon_dec_min = float(lon_ddm_str[4:])*100
        longitude = lon_deg-lon_dec_min/60
        longitude = round(longitude,7)
        print(longitude)
        node_str = 2                               
        node_object = Node(noi_id,latitude,longitude, status, node_path, 0)
        return node_object


### Status Message Response if Node found:
'''
-RECEIVED NODE MSG [rssi: -32]-
msg_type:               ACK_STATUS_NODE_FOUND
source_node:            5
target_node:            1
payload_size:           12
payload:                0x5 0x0 0x1 0xEE 0xBA 0x34 0xF9 0x4 0x8A 0x45 0x1 0x5
node_of_interest:       5
status:                 0
lat:                    32422452
lon:                    -117142971
num_nodes_on_path:      2
nodes_on_path:          1 5
'''
