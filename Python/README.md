### The Host program may executed by entering "sudo python host.py" within the "Python" directory of the Trail Beacons Repository on the Raspberry Pi.  

### Major functions and features of the python host program are:
	
	1. Provide serial interface to micro control layer by reading and writing serial data. 
	
	2. Parse and interpret status messages from Micro layer and convert to node object attributes. 
	
	3. Provide JSON interface to webpage layer. 
	
	4. Conversion of degree decimal minutes (DDM) to degree decimal (DD). This is done because the webpage map interface supports DD. 
    
	5. Recognize when nodes leave and join the network and update node object attributes accordingly. 
	
	6. Recognize when GPS on nodes is warming up. 
	
	7. Mitigate off-the-shelf GPS false location bug.  Every so often the GPS board on the Micro stack will provide an unrealistic location; the Python layer mitigates the bug by filtering the false locations. 
	
The Python folder of the repository consists of 3 files: host.py, read_status.py, and node_info.py.  
host.py is the file that is executed at the host raspberry pi terminal.  The host.py loops through the possible node list, controls the Feather M0 Microcontroller layer over serial.  It also handles the node object list as well as JSON handling.  Node_info.py is called once at the beginning of the program to retrieve and interpret the host node location.  read_status.py is called whenever a status is received back from a node after a request.  The read_status function interprets and processes the serial status message from the node, and stores it in a node object.
	

