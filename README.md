# **WES C7 Trail Beacons**

> Trail Beacons is node relay system designed to be a cost effective solution to location tracking for hikers. Typical solutions for tracking location involve costly GPS units with satellite communication to request for help when assistance is needed. Our goal is to provide an alternative unit using significantly cheaper radios operating in the ISM band to provide the comprable functionality. With a custom messaging scheme we will be able to extend the communication range of a standard radio by "node-hopping" between radios to deliver and gather information to a hosting unit. Therefore by using the units carried by fellow hikers or possibly strategically placed units along the trail, we can track locations in the event of an emergency.

**========================================**

## **Node Hardware**

### Feather M0 RFM69
At the heart of each nodes is the Adafruit Feather M0 RFM69 that uses a SAMD21 M0 microcontroller and the 915MHz RFM69HCW half-duplex transceiver.

The Feather M0 RFM69 runs off the of the custom node network protocol that enables all nodes to communicate on a dispersed network. Details on the node network can be found in the Doc folder. Firmware for the nodes can be found in the Hardware folder.

![pic](img/feather_3176_iso_ORIG.jpg)
https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio/overview

### Adafruit GPS Feather
The Adafruit GPS Feather enables NMEA0183 GPS tracking capabilities. This low cost GPS receiver is the most expensive item in the node and provides position tracking of each node.

![pic](img/gps_3133_top_ORIG.jpg)
https://learn.adafruit.com/adafruit-ultimate-gps-featherwing/overview

### Adafruit OLED Feather
The Adafruit OLED Feather provides the user with node status information. The user can use the buttons on the OLED board to update their status or view their own status such as their own status selection and the GPS time/position.

![pic](img/feather_2900-02.jpg)
https://learn.adafruit.com/adafruit-oled-featherwing?view=all

### Adafruit Adalogger Feather
The Adafruit Adalogger Feather allows the node to read its configuration settings from a file. Information will be written to the SD card while the node is in a powered state to be processed once returned if necessary.

![pic](img/feather_2922-05.jpg)
https://learn.adafruit.com/adafruit-adalogger-featherwing 

### FeatherWing Tripler Mini Kit - Prototyping Add-on
Brings the components together in a more presentable arrangement. Provides a convenient method to connect each of the components without using something like a breadboard and wires (nothing against breadboards/wires, just saying _it's not the most aesthetically pleasing device to look at..._ annnd moving on)

![pic](img/Adafruit_Feather_Tripler.jpg)
https://www.adafruit.com/product/3417

### Arduino IDE
Tool used upload code to the board. Arduino IDE provides a convenient environment for managing board libraries and compiling code. 

This is how you setup the Arduino IDE:
https://learn.adafruit.com/adafruit-feather-32u4-radio-with-rfm69hcw-module/setup

And incorperate all the proper libraries:
https://learn.adafruit.com/adafruit-feather-32u4-radio-with-rfm69hcw-module/using-with-arduino-ide


**========================================**

## **Raspberry Pi**
Running the latest Raspbian release [Raspbian Stretch](https://www.raspberrypi.org/downloads/raspbian/)

The Raspberry Pi installed with Python3 controls the host node requesting node information in the network.  A description of the Python Host Program can be found below:
### Python Host Program
The Python Host program is the brain behind the system, forming the glue between the Feather Microcontroller C layer and the Webpage User Interface. 

The host is a python software process that controls the Trail Beacons network by sending serial data to the host Feather M0 microcontroller from the Raspberry Pi and initiating the Feather M0 firmware execution.
Serial data is received back from the M0 micro-controller, processed, and packaged in JSON for the webpage layer.  
The status data of each node is stored as an object in a Python list, which is dumped to a JSON file.  The attributes of the objects are node ID, latitude, longitude, status, and routing path.

The host operates by looping through the max number of nodes allowed on the network and requesting status from each node.  In each loop iteration, the information from each node is read, processed, and packaged into JSON for the webpage layer.
The full operation of the host is described in detail in the diagram below:

![pic](img/Host_State_Diagram_v1.PNG)

### A screenshot of the Raspberry Pi host terminal with the host program running may be seen below.
The Host program may executed by entering "sudo python host.py" within the "Python" directory of the Trail Beacons Repository on the Raspberry Pi.  

![pic](img/host_terminal.PNG)

### Major functions and features of the python host program are:
	
	1. Provide serial interface to micro control layer by reading and writing serial data. 
	
	2. Parse and interpret status messages from Micro layer and convert to node object attributes. 
	
	3. Provide JSON interface to webpage layer. 
	
	4. Conversion of degree decimal minutes (DDM) to degree decimal (DD). This is done because the webpage map interface supports DD. 
    
	5. Recognize when nodes leave and join the network and update node object attributes accordingly. 
	
	6. Recognize when GPS on nodes is warming up. 
	
	7. Mitigate off-the-shelf GPS false location bug.  Every so often the GPS board on the Micro stack will provide an unrealistic location; the Python layer mitigates the bug by filtering the false locations. 
	
The Python folder of the repository consists of 3 files: host.py, read_status.py, and node_info.py.  
host.py is the file that is executed at the host raspberry pi terminal.  The host.py loops through the possible node list, controls the Feather M0 Microcontroller layer over serial.  It also handles the node object list as well as JSON handling.  Node_info.py is called once at the beginning of the program to retrieve and interpret the host node location.  read_status.py is called whenever a status is received back from a node after a request.  The read_status function interprets and processes the serial status message from the node, and stores it in a node object.
	
	
### NGINX
Open source web server for hosting the web page with our primary graphical representation of the application.

NGINX is relatively simple to use and is light enough to run in the back ground on the Raspberry Pi. Follow the instuctions to install the service.

https://www.raspberrypi.org/documentation/remote-access/web-server/nginx.md

After installation, place your website files (html, js, css, ...) in the /var/www/html/ folder. The page will be available to anyone on the same network by simply typing the IP address into the browser

### Leaflet
Open source JavaScript library used to generate and control the map portion of the page. By learning to work with the layer groups/control design the library uses, different shapes, pop-ups, markers, etc can be placed on the map changing how users interact with it. The library offers several different map styles to show geospatial information emphasizing different aspects of an area.

![pic](img/screen3.png)
https://leafletjs.com/

**Note: Folium**
Folium is essentially a python wrapper for Leaflet. While we did not end up using it in the final product, it is worth metioning as it works well with displaying maps within Jupyter Notebooks. It works great with generating static maps as a lot of functionality is present in the library. Dynamic addition and removal of some elements was not possible so we transitioned directly to Leaflet.

https://python-visualization.github.io/folium/

**========================================**

## **System Description**

Below is a diagram representing the communication path within the Host Node. Other nodes will not have the serial connection the Raspberry Pi. 

![pic](img/stack.png)

From right to left:
1. NGINX Web server hosts the Trail Beacons web site
2. Webpage Layer receives JSON formatted data from the RPi host program
3. Python Layer runs the host program to form JSON formatted data based on messages received at the designated host node
4. Feather Arm Cortex M0 sends data via serial to the RPi once requested by the host program
5. C Microcontroller Layer handles information gathered from attached peripherals 
6. Radio transmissions are sent/received between nodes via the Transceiver 

![pic](img/msg1.png)

The general message format is realtively straight forward. Each message specifies a source address, destination address, and target address. We currently have 8 bits designated for each node address in our messaging scheme. So potentially, we support up to 255 unique node IDs and 255 devices (not including an ID of 0). Following that we specify a message type and then size of the payload for the remainder of the message. Using this as the basis, for communication we propogate node information between nodes and back to the node we have spcificed as the host. 

Image illustrating how information is passed from node 8 is passed to node 1

![pic](img/example1.png)
