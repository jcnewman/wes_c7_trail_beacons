#include "RFM.h"

//this_node is the info pertaining to this node.
//When constructing a data packet, this node
//is also the SOURCE_NODE.
node this_node;

// Singleton instance of the radio driver
RH_RF69 RFM69(RFM_CS_PIN, RFM_INT_PIN);

bool RFM_LED;
msg tx_msg;
msg rcv_msg;
route node_route;


/*
FUNC:		RFM_Init
INPUT:		void
OUTPUT:		void
PURPOSE:	Initializes the RFM69 hardware on the Feather M0.
			Enables communication to the RFM69 and enables LED.
*/
void RFM_Init(uint8_t this_node_addr, float freq)
{
	pinMode(RFM_LED_PIN, OUTPUT);
	RFM_LED_State(false);
	
	pinMode(RFM_RST_PIN, OUTPUT);
	digitalWrite(RFM_RST_PIN, LOW);
	
	RFM_Reset();

	this_node.addr = this_node_addr;
	this_node.freq = freq;
	
	this_node.init = true;
	
	
	//If init fails, then this_node.init will be false.
	//If this_node.init is false, no message will be sent.
	if (!RFM69.init())
	{
		this_node.init = false;
		RFM_LED_State(true);
	}
	// Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
	// No encryption
	if (!RFM69.setFrequency(this_node.freq))
	{
		this_node.init = false;
		RFM_LED_State(true);
	}
	
	//Continue with RFM69 HW setup if init correctly.
	if(this_node.init)
	{
		// If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
		// is highpowermodule flag set like this:
		RFM69.setTxPower(TRANSMIT_PWR_LVL, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

		uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
		RFM69.setEncryptionKey(key);
	}
	
	
}


/*
FUNC:		RFM_Reset
INPUT:		void
OUTPUT:		void
PURPOSE:	Resets the state of all pins and this_node fields.
			Resets the RFM69 hardware by toggling the reset pin.
*/
void RFM_Reset(void)
{
	digitalWrite(RFM_RST_PIN, HIGH);
	delay(RFM_RESET_PIN_DELAY);
	digitalWrite(RFM_RST_PIN, LOW);
	delay(RFM_RESET_PIN_DELAY);
	
	this_node.init = false;
	this_node.addr = 0;
	this_node.freq = 915.00;
	this_node.rx_msg = MSG_NO_MSG;
	this_node.tx_msg = MSG_NO_MSG;
	this_node.lat = 32.704019*10000;
	this_node.lon = -117.238378*10000;
	this_node.stat = STATUS_GOOD;
	
	RFM_ResetMsg(&tx_msg);
	tx_msg.source_node = this_node.addr;
	
	RFM_ResetMsg(&rcv_msg);
	
	node_route.num_nodes_on_path = 0;
	node_route.node_of_interest = 0;
	node_route.check_target_node = 2;	//Starts from 2, 0 is special and 1 is host.
	memset(node_route.nodes_on_path, NODES_ON_PATH_SIZE, 0x00);
}

/*
FUNC:     RFM_Avail
INPUT:    void
OUTPUT:   bool - true if bytes in RFM69 buffer, false else.
PURPOSE:  Determines if there are bytes in the RFM69 hardware buffer.
*/
bool RFM_Avail(void)
{
  return RFM69.available();
}

/*
FUNC:		RFM_SendMsg
INPUT:		msg* msg_to_rcv - pointer to msg struct of received msg.
OUTPUT:		void
PURPOSE:	Sends a well formed node message.
*/
void RFM_SendMsg(msg* msg_to_tx)
{	
	uint8_t tx_buff[PAYLOAD_BUFF_SIZE + MSG_HEADER_SIZE];
	
	tx_buff[0] = SENTINEL;
	tx_buff[1] = msg_to_tx->source_node;
	tx_buff[2] = msg_to_tx->target_node;
	tx_buff[3] = msg_to_tx->msg_type;
	tx_buff[4] = msg_to_tx->payload_size;
	
	for(int i = 0; i < msg_to_tx->payload_size; i++)
	{
		tx_buff[5 + i] = msg_to_tx->payload_buff[i];
	}
	
	if(this_node.init)
	{
		RFM69.send(tx_buff, msg_to_tx->payload_size + MSG_HEADER_SIZE);
		RFM69.waitPacketSent();
	}
	else
	{
		RFM_LED_Blink(6, LED_ERROR_DLY);
		Serial.println("Error - RFM69 not init successfully.");
	}
	
	
}


/*
FUNC:		RFM_RcvMsg
INPUT:		msg* msg_to_rcv - pointer to msg struct of received msg.
OUTPUT:		void
PURPOSE:	Checks if received data is a well formed node message.
			Parses for header information including SOURCE_NODE, TARGET_NODE and PAYLOAD.
			RFM_RcvMsg does not act on the msg received.
*/
void RFM_RcvMsg(msg* msg_to_rcv)
{
	uint8_t buff[PAYLOAD_BUFF_SIZE + MSG_HEADER_SIZE];
	uint8_t buff_len = sizeof(buff);
	bool msg_found = false;
	
	if(RFM69.recv(buff, &buff_len))
	{
		//Received a message of non-zero length
		if(buff_len != 0)
		{
			//data may be a well formed node msg.
			if(buff[0] == '@' && buff[4] <= PAYLOAD_BUFF_SIZE && buff[4] == buff_len - 5)
			{	
				//Serial.println("Msg received - Well formed node msg.");
				msg_found = true;
			}
			//data was not a well formed node msg.
			else
			{
				//Serial.println("Msg received - Not a well formed node msg.");
				msg_found = false;
			}
		}
		//Parse the header info if the data was a well formed node msg.
		if(msg_found)
		{
			msg_to_rcv->source_node = buff[1];	//SOURCE_NODE
			msg_to_rcv->target_node = buff[2];	//TARGET_NODE
			msg_to_rcv->msg_type = buff[3];		//MSG_TYPE
			msg_to_rcv->payload_size = buff[4];	//PAYLOAD_SIZE
			
			//Copy payload byte for byte.
			for(int i = 0; i < msg_to_rcv->payload_size; i++)
			{
				msg_to_rcv->payload_buff[i] = buff[5 + i];
			}
			
			//RSSI only for receive message from SOURCE_NODE
			msg_to_rcv->rssi = RFM69.lastRssi();
			
			//If the msg is a MSG_REQ_AVAIL or MSG_ACK_AVAIL, check for thresh.
			if(msg_to_rcv->rssi > RFM_RCV_ACK_THRESH && (msg_to_rcv->msg_type == MSG_REQ_AVAIL || msg_to_rcv->msg_type == MSG_ACK_AVAIL))
			{
				msg_to_rcv->msg_rcvd_success = true;
				//Serial.println("ACK within threshold.\n");
			
			}
			else if(msg_to_rcv->rssi > RFM_RCV_MSG_THRESH && msg_to_rcv->msg_type != MSG_REQ_AVAIL && msg_to_rcv->msg_type != MSG_ACK_AVAIL)
			{
				msg_to_rcv->msg_rcvd_success = true;
				//Serial.println("MSG within threshold.\n");
			}
			else
			{
				msg_to_rcv->msg_rcvd_success = false;
				//Serial.println("ACK outside threshold.\n");
			}
			
		}
	}
}

/*
FUNC:		RFM_ProcessRcvMsg
INPUT:		msg* msg_rcvd - The well formed received node message.
OUTPUT:		void
PURPOSE:	Take a well formed node message and determines what needs to happen
			base on the message.
*/
void RFM_ProcessRcvMsg(msg* msg_rcvd)
{
	//Variable placeholders for incoming data
	signed long int lat = 0;
	signed long int lon = 0;
	uint8_t stat = 0x00;
	uint8_t next_node = 0;
	bool msg_sent = false;
	
	RFM_LED_State(!RFM_LED);
	
	if(msg_rcvd->target_node == this_node.addr && msg_rcvd->msg_rcvd_success)
	{
		//Serial.println("Node msg meant for this node.");
		//Serial.println("Received msg:");
		
		RFM_ShowMsg(msg_rcvd, true);
		
		if(msg_rcvd->msg_type == MSG_REQ_AVAIL)
		{	
			tx_msg.source_node = this_node.addr;
			tx_msg.target_node = msg_rcvd->source_node;
			tx_msg.msg_type = MSG_ACK_AVAIL;
			tx_msg.payload_size = 0;
			
			RFM_ShowMsg(&tx_msg, false);
			RFM_SendMsg(&tx_msg);
		}
		
		else if(msg_rcvd->msg_type == MSG_ACK_AVAIL)
		{
			//Nothing needs to be done here.
		}
		
		else if(msg_rcvd->msg_type == MSG_REQ_STATUS_NODE)
		{	
			//Retrieve node list data from payload.
			node_route.node_of_interest = msg_rcvd->payload_buff[0];
			node_route.num_nodes_on_path = (msg_rcvd->payload_size) - 1;
			
			for(int i = 0; i < node_route.num_nodes_on_path; i++)
			{
				node_route.nodes_on_path[i] = msg_rcvd->payload_buff[i+1];
			}
			
			//I have received a request for REQ_STATUS_NODE.
			//I am the NODE_OF_INTEREST or I need to look for the NODE_OF_INTEREST.
			//Either way, I need to add my own addr to the list.
			node_route.num_nodes_on_path = node_route.num_nodes_on_path + 1;
			node_route.nodes_on_path[node_route.num_nodes_on_path - 1] = this_node.addr;
			
			//if the NODE_OF_INTEREST is this_node, then reply with ACK_STATUS_NODE_FOUND
			if(this_node.addr == node_route.node_of_interest)
			{
				Serial.println("This node is the NODE_OF_INTEREST");
				Serial.println("");
				
				lat = node_gps.lat;
				lon = node_gps.lon;
				
				//Construct msg to backtrack the node list.
				tx_msg.source_node = this_node.addr;
				tx_msg.target_node = node_route.nodes_on_path[node_route.num_nodes_on_path - 2];
				tx_msg.msg_type = MSG_ACK_STATUS_NODE_FOUND;
				tx_msg.payload_size = node_route.num_nodes_on_path + MSG_STATUS_SIZE;	//MSG_STATUS_SIZE bytes for stats
				tx_msg.payload_buff[0] = node_route.node_of_interest;
				tx_msg.payload_buff[1] = this_node.stat;
				
				tx_msg.payload_buff[2] = (lat>>24) & 0xFF;
				tx_msg.payload_buff[3] = (lat>>16) & 0xFF;
				tx_msg.payload_buff[4] = (lat>>8) & 0xFF;
				tx_msg.payload_buff[5] = (lat>>0) & 0xFF;
				
				tx_msg.payload_buff[6] = (lon>>24) & 0xFF;
				tx_msg.payload_buff[7] = (lon>>16) & 0xFF;
				tx_msg.payload_buff[8] = (lon>>8) & 0xFF;
				tx_msg.payload_buff[9] = (lon>>0) & 0xFF;
				
				for(int i = 0; i < node_route.num_nodes_on_path; i++)
				{
					tx_msg.payload_buff[MSG_STATUS_SIZE + i] = node_route.nodes_on_path[i];
				}
				
				RFM_ShowMsg(&tx_msg, false);
				RFM_SendMsg(&tx_msg);
				
			}
			
			//NODE_OF_INTEREST is not this_node.
			//Start looking for the next node to pass msg to.
			else
			{
				Serial.println("This is not the node you're looking for.");
				
				//Check if NODE_OF_INTEREST in range first.
				if(PERFORM_SEARCH_NODE_OF_INTEREST_FIRST)
				{
					if(RFM_IsNodeAvail(&tx_msg, node_route.node_of_interest))
					{
						//NODE_OF_INTEREST is within range.
						tx_msg.source_node = this_node.addr;
						tx_msg.target_node = node_route.node_of_interest;
						tx_msg.msg_type = MSG_REQ_STATUS_NODE;
						tx_msg.payload_size = node_route.num_nodes_on_path + 1;	//1 byte for node of interest
						tx_msg.payload_buff[0] = node_route.node_of_interest;
						
						for(int i = 0; i < node_route.num_nodes_on_path; i++)
						{
							tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
						}
						
						RFM_ShowMsg(&tx_msg, false);
						RFM_SendMsg(&tx_msg);
						
						msg_sent = true;
						
					}
				}
								
				//Will keep trying until msg is sent
				while(!msg_sent)
				{
					node_route.check_target_node = (node_route.check_target_node) + 1;
					//Ensures we do not move backwards on the path when checking for nodes.
					for(int i = 0; i < node_route.num_nodes_on_path; i++)
					{
						//checks to see if the check_target_node is this node
						if(node_route.check_target_node == this_node.addr)
						{
							node_route.check_target_node = node_route.check_target_node + 1;
						}
						
						//Checks to see if the check_target_node is on the path
						if(node_route.check_target_node == node_route.nodes_on_path[i])
						{
							node_route.check_target_node = node_route.check_target_node + 1;
						}
					}
					
					if(node_route.check_target_node < NODES_ON_PATH_SIZE)
					{
						if(RFM_IsNodeAvail(&tx_msg, node_route.check_target_node))
						{
							tx_msg.source_node = this_node.addr;
							tx_msg.target_node = node_route.check_target_node;
							tx_msg.msg_type = MSG_REQ_STATUS_NODE;
							tx_msg.payload_size = node_route.num_nodes_on_path + 1;	//1 byte for node of interest
							tx_msg.payload_buff[0] = node_route.node_of_interest;
							
							for(int i = 0; i < node_route.num_nodes_on_path; i++)
							{
								tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
							}
							
							RFM_ShowMsg(&tx_msg, false);
							RFM_SendMsg(&tx_msg);
							
							msg_sent = true;
						}
						/*
						//Check if node is in broadcast range
						tx_msg.source_node = this_node.addr;
						tx_msg.target_node = node_route.check_target_node;
						tx_msg.msg_type = MSG_REQ_AVAIL;
						tx_msg.payload_size = 0;
						
						RFM_ShowMsg(&tx_msg, false);
						RFM_SendMsg(&tx_msg);
						
						
						for(uint8_t wait_for_response = 0; wait_for_response < ACK_RESPONSE_ATTEMPT; wait_for_response++)
						{
							if(RFM_Avail())
							{
								RFM_RcvMsg(&rcv_msg);
								RFM_ProcessRcvMsg(&rcv_msg);
								
								//If node is in broadcast range, form a REQ_STATUS_NODE and send
								if(rcv_msg.target_node == this_node.addr && rcv_msg.source_node == tx_msg.target_node && rcv_msg.msg_type == MSG_ACK_AVAIL)
								{
									tx_msg.source_node = this_node.addr;
									tx_msg.target_node = node_route.check_target_node;
									tx_msg.msg_type = MSG_REQ_STATUS_NODE;
									tx_msg.payload_size = node_route.num_nodes_on_path + 1;	//1 byte for node of interest
									tx_msg.payload_buff[0] = node_route.node_of_interest;
									
									for(int i = 0; i < node_route.num_nodes_on_path; i++)
									{
										tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
									}
									
									RFM_ShowMsg(&tx_msg, false);
									RFM_SendMsg(&tx_msg);
									
									msg_sent = true;
									
									break;
								}
							}
							
							delay(ACK_RESPONSE_DLY);
						}
						*/
					}
					//All possible node list exhausted, return ACK_STATUS_NODE_NOT_FOUND
					else
					{
						node_route.nodes_on_path[(node_route.num_nodes_on_path) - 1] = 0x00;				//clear this node addr from the nodes_on_path list
						node_route.num_nodes_on_path = (node_route.num_nodes_on_path) - 1;	//decrement the total number of nodes on path
						
						tx_msg.source_node = this_node.addr;
						tx_msg.target_node = node_route.nodes_on_path[(node_route.num_nodes_on_path) - 1];
						tx_msg.msg_type = MSG_ACK_STATUS_NODE_NOT_FOUND;
						tx_msg.payload_size = node_route.num_nodes_on_path + 1;				//Node of interest and the total nodes on path
						tx_msg.payload_buff[0] = node_route.node_of_interest;
						
						for(int i = 0; i < node_route.num_nodes_on_path; i++)
						{
							tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
						}
						
						RFM_ShowMsg(&tx_msg, false);
						RFM_SendMsg(&tx_msg);
						
						msg_sent = true;
						
						node_route.check_target_node = 1;	//resets the next node to check
					
					}
					
				}
				
			}

			
		}
		else if(msg_rcvd->msg_type == MSG_ACK_STATUS_NODE_FOUND)
		{
			node_route.check_target_node = 1;	//resets the next node to check
			
			//Resets ndoes on path, will be populated with new ones.
			memset(node_route.nodes_on_path, NODES_ON_PATH_SIZE, 0x00);
			
			node_route.node_of_interest = msg_rcvd->payload_buff[0];
			stat = msg_rcvd->payload_buff[1];
			lat = (msg_rcvd->payload_buff[2] << 24 | msg_rcvd->payload_buff[3] << 16 | msg_rcvd->payload_buff[4] << 8 | msg_rcvd->payload_buff[5]);
			lon = (msg_rcvd->payload_buff[6] << 24 | msg_rcvd->payload_buff[7] << 16 | msg_rcvd->payload_buff[8] << 8 | msg_rcvd->payload_buff[9]);
			node_route.num_nodes_on_path = msg_rcvd->payload_size - MSG_STATUS_SIZE;
			
			for(int i = 0; i < node_route.num_nodes_on_path; i++)
			{
				node_route.nodes_on_path[i] = msg_rcvd->payload_buff[MSG_STATUS_SIZE + i];
				
				//Looks at the nodes_on_path, determine this_node's current location
				//and figure which node it needs to transmit next.
				if(node_route.nodes_on_path[i] == this_node.addr && i != 0 && this_node.addr != HOST_ADDR)
				{
					next_node = node_route.nodes_on_path[i - 1];
				}
			}
			
			if(this_node.addr != HOST_ADDR)
			{
				//Prepare transmit message
				tx_msg.source_node = this_node.addr;
				tx_msg.target_node = next_node;
				tx_msg.msg_type = MSG_ACK_STATUS_NODE_FOUND;
				tx_msg.payload_size = node_route.num_nodes_on_path + MSG_STATUS_SIZE;	//10 bytes for stats
				tx_msg.payload_buff[0] = node_route.node_of_interest;
				tx_msg.payload_buff[1] = stat;
				
				tx_msg.payload_buff[2] = (lat>>24) & 0xFF;
				tx_msg.payload_buff[3] = (lat>>16) & 0xFF;
				tx_msg.payload_buff[4] = (lat>>8) & 0xFF;
				tx_msg.payload_buff[5] = (lat>>0) & 0xFF;
				
				tx_msg.payload_buff[6] = (lon>>24) & 0xFF;
				tx_msg.payload_buff[7] = (lon>>16) & 0xFF;
				tx_msg.payload_buff[8] = (lon>>8) & 0xFF;
				tx_msg.payload_buff[9] = (lon>>0) & 0xFF;
			
				for(int i = 0; i < node_route.num_nodes_on_path; i++)
				{
					tx_msg.payload_buff[MSG_STATUS_SIZE + i] = node_route.nodes_on_path[i];
				}
				
				RFM_ShowMsg(&tx_msg, false);
				RFM_SendMsg(&tx_msg);
			}
		}
		else if(msg_rcvd->msg_type == MSG_ACK_STATUS_NODE_NOT_FOUND)
		{
			//Resets ndoes on path, will be populated with new ones.
			memset(node_route.nodes_on_path, NODES_ON_PATH_SIZE, 0x00);
			
			//Retrieve node list data from payload.
			//The node that could not find the NODE_OF_INTEREST is already trimmed off before sending.
			node_route.node_of_interest = msg_rcvd->payload_buff[0];
			node_route.num_nodes_on_path = (msg_rcvd->payload_size) - 1;
			
			for(int i = 0; i < node_route.num_nodes_on_path; i++)
			{
				node_route.nodes_on_path[i] = msg_rcvd->payload_buff[i+1];
			}
			
			
			Serial.print("Node ");
			Serial.print(node_route.check_target_node, DEC);
			Serial.print(" could not find node ");
			Serial.println(node_route.node_of_interest, DEC);
				
			//Will keep trying until msg is sent
			while(!msg_sent)
			{
				node_route.check_target_node = (node_route.check_target_node) + 1;
				//Ensures we do not move backwards on the path when checking for nodes.
				for(int i = 0; i < node_route.num_nodes_on_path; i++)
				{
					//checks to see if the check_target_node is this node
					if(node_route.check_target_node == this_node.addr)
					{
						node_route.check_target_node = node_route.check_target_node + 1;
					}
					
					//Checks to see if the check_target_node is on the path
					if(node_route.check_target_node == node_route.nodes_on_path[i])
					{
						node_route.check_target_node = node_route.check_target_node + 1;
					}
				}
				
				if(node_route.check_target_node < NODES_ON_PATH_SIZE)
				{
					if(RFM_IsNodeAvail(&tx_msg, node_route.check_target_node))
					{
						tx_msg.source_node = this_node.addr;
						tx_msg.target_node = node_route.check_target_node;
						tx_msg.msg_type = MSG_REQ_STATUS_NODE;
						tx_msg.payload_size = node_route.num_nodes_on_path + 1;	//1 byte for node of interest
						tx_msg.payload_buff[0] = node_route.node_of_interest;
						
						for(int i = 0; i < node_route.num_nodes_on_path; i++)
						{
							tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
						}
						
						RFM_ShowMsg(&tx_msg, false);
						RFM_SendMsg(&tx_msg);
						
						msg_sent = true;
					}
					
					/*
					//Check if node is in broadcast range
					tx_msg.source_node = this_node.addr;
					tx_msg.target_node = node_route.check_target_node;
					tx_msg.msg_type = MSG_REQ_AVAIL;
					tx_msg.payload_size = 0;
					
					RFM_ShowMsg(&tx_msg, false);
					RFM_SendMsg(&tx_msg);
					
					for(uint8_t wait_for_response = 0; wait_for_response < ACK_RESPONSE_ATTEMPT; wait_for_response++)
					{
						if(RFM_Avail())
						{
							RFM_RcvMsg(&rcv_msg);
							RFM_ProcessRcvMsg(&rcv_msg);
							
							//If node is in broadcast range, form a REQ_STATUS_NODE and send
							if(rcv_msg.target_node == this_node.addr && rcv_msg.source_node == tx_msg.target_node && rcv_msg.msg_type == MSG_ACK_AVAIL)
							{
								tx_msg.source_node = this_node.addr;
								tx_msg.target_node = node_route.check_target_node;
								tx_msg.msg_type = MSG_REQ_STATUS_NODE;
								tx_msg.payload_size = node_route.num_nodes_on_path + 1;	//1 byte for node of interest
								tx_msg.payload_buff[0] = node_route.node_of_interest;
								
								for(int i = 0; i < node_route.num_nodes_on_path; i++)
								{
									tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
								}
								
								RFM_ShowMsg(&tx_msg, false);
								RFM_SendMsg(&tx_msg);
								
								msg_sent = true;
								
								break;
							}
						}
						
						delay(ACK_RESPONSE_DLY);
					}
					*/
				}
				//All possible node list exhausted, return ACK_STATUS_NODE_NOT_FOUND
				else
				{
					if(this_node.addr != HOST_ADDR)
					{
						
						node_route.nodes_on_path[(node_route.num_nodes_on_path) - 1] = 0x00;				//clear this node addr from the nodes_on_path list
						node_route.num_nodes_on_path = (node_route.num_nodes_on_path) - 1;	//decrement the total number of nodes on path
						
						tx_msg.source_node = this_node.addr;
						tx_msg.target_node = node_route.nodes_on_path[(node_route.num_nodes_on_path) - 1];
						tx_msg.msg_type = MSG_ACK_STATUS_NODE_NOT_FOUND;
						tx_msg.payload_size = node_route.num_nodes_on_path + 1;				//Node of interest and the total nodes on path
						tx_msg.payload_buff[0] = node_route.node_of_interest;
						
						for(int i = 0; i < node_route.num_nodes_on_path; i++)
						{
							tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
						}
						
						RFM_ShowMsg(&tx_msg, false);
						RFM_SendMsg(&tx_msg);
						
						msg_sent = true;
						
						node_route.check_target_node = 1;	//resets the next node to check
					}
					else
					{
						Serial.print("Node ");
						Serial.print(node_route.node_of_interest, DEC);
						Serial.println(" is not in the network");
						
						msg_sent = true;
						node_route.check_target_node = 1;
					}
				}
			}
		}
		else
		{
			Serial.println("Unknown node msg.");
		}
		
		
	}
	
	else
	{
		//Serial.println("Node msg meant for another node.");
		//Serial.println("");
	}
	
	
	
}

/*
FUNC:		RFM_ShowMsg
INPUT:		msg* show_msg - pointer to msg struct of node msg to show.
			bool rcv - true if msg is inbound and false for outbound.
OUTPUT:		void
PURPOSE:	Displays well formed node msgs for both recieved and
			transmitted msgs.
*/
void RFM_ShowMsg(msg* show_msg, bool rcv)
{
	signed long int lat = 0;
	signed long int lon = 0;
	
	//Inbound or outbout node msg.

	if(rcv)
	{
		Serial.print("-RECEIVED NODE MSG [rssi: ");
		Serial.print(show_msg->rssi, DEC);
		Serial.println("]-");
		
	}
	else
	{
		Serial.println("-TRANSMIT NODE MSG-");
	}
	
	//msg_type
	Serial.print("msg_type:\t\t");
	switch(show_msg->msg_type)
	{
		case MSG_REQ_AVAIL:
			Serial.println("REQ_AVAIL");
			break;
		case MSG_ACK_AVAIL:
			Serial.println("ACK_AVAIL");
			break;
		case MSG_REQ_STATUS_NODE:
			Serial.println("REQ_STATUS_NODE");
			break;
		case MSG_ACK_STATUS_NODE_NOT_FOUND:
			Serial.println("ACK_STATUS_NODE_NOT_FOUND");
			break;
		case MSG_ACK_STATUS_NODE_FOUND:
			Serial.println("ACK_STATUS_NODE_FOUND");
			break;
		default:
			Serial.println("MSG_NOT_SUPPORTED");
			break;
	}
	
	//source_node and target_node
	Serial.print("source_node:\t\t");
	Serial.println(show_msg->source_node);
	Serial.print("target_node:\t\t");
	Serial.println(show_msg->target_node);
	
	//payload_size
	Serial.print("payload_size:\t\t");
	Serial.println(show_msg->payload_size);
	
	//Show payload
	if(show_msg->payload_size == 0)
	{
		Serial.println("payload:\t\tEMPTY");
	}
	else
	{
		Serial.print("payload:\t\t");

		for(int i = 0; i < show_msg->payload_size; i++)
		{
			if(i != 0)
			{
				Serial.print(" 0x");
			}
			else
			{
				Serial.print("0x");
			}
			Serial.print(show_msg->payload_buff[i], HEX);
		}
		Serial.println("");
	}
	
	//Show additional if needed
	switch(show_msg->msg_type)
	{
		case MSG_REQ_AVAIL:
			//Nothing to show
			break;
		case MSG_ACK_AVAIL:
			//Nothing to show
			break;
		case MSG_REQ_STATUS_NODE:
			Serial.print("node_of_interest:\t");
			Serial.println(show_msg->payload_buff[0]);
			Serial.print("num_nodes_on_path:\t");
			Serial.println((show_msg->payload_size) - 1, DEC);
			Serial.print("nodes_on_path:\t\t");
			for(int i = 0; i < (show_msg->payload_size) - 1; i++)
			{
				if(i != 0)
				{
					Serial.print(" ");
				}
				Serial.print(show_msg->payload_buff[1 + i]);
			}
			Serial.println("");
			break;
		case MSG_ACK_STATUS_NODE_NOT_FOUND:
			Serial.print("node_of_interest:\t");
			Serial.println(show_msg->payload_buff[0]);
			Serial.print("num_nodes_on_path:\t");
			Serial.println((show_msg->payload_size) - 1, DEC);
			Serial.print("nodes_on_path:\t\t");
			for(int i = 0; i < (show_msg->payload_size) - 1; i++)
			{
				if(i != 0)
				{
					Serial.print(" ");
				}
				Serial.print(show_msg->payload_buff[1 + i]);
			}
			Serial.println("");
			break;
		case MSG_ACK_STATUS_NODE_FOUND:
			Serial.print("node_of_interest:\t");
			Serial.println(show_msg->payload_buff[0]);
			Serial.print("status:\t\t\t");
			Serial.println(show_msg->payload_buff[1]);
			
			lat = ((show_msg->payload_buff[2])<<24 | (show_msg->payload_buff[3])<<16 | (show_msg->payload_buff[4])<<8 | (show_msg->payload_buff[5]));
			Serial.print("lat:\t\t\t");
			Serial.println(lat);
			
			lon = ((show_msg->payload_buff[6])<<24 | (show_msg->payload_buff[7])<<16 | (show_msg->payload_buff[8])<<8 | (show_msg->payload_buff[9]));
			Serial.print("lon:\t\t\t");
			Serial.println(lon);
			
			Serial.print("num_nodes_on_path:\t");
			Serial.println((show_msg->payload_size) - MSG_STATUS_SIZE, DEC);
			Serial.print("nodes_on_path:\t\t");
			for(int i = 0; i < (show_msg->payload_size) - MSG_STATUS_SIZE; i++)
			{
				if(i != 0)
				{
					Serial.print(" ");
				}
				Serial.print(show_msg->payload_buff[MSG_STATUS_SIZE + i]);
			}
			Serial.println("");
			
			break;
		default:
			break;
	}
	
	//Finished
	Serial.println("");
	
}

/*
FUNC:		RFM_IsNodeAvail
INPUT:		msg* msg_to_tx 		- The node message to transmit with.
			uint8_t target_node - The node address to transmit.
OUTPUT:		bool node_avail		- Is node available? True if so false if not.
PURPOSE:	Determines if target_node is available by transmitting a MSG_REQ_AVAIL
			and waiting for a MSG_ACK_AVAIL from target_node.
*/
bool RFM_IsNodeAvail(msg* msg_to_tx, uint8_t target_node)
{
	bool node_avail = false;
	
	//Do a quick search for NODE_OF_INTEREST first
	msg_to_tx->source_node = this_node.addr;
	msg_to_tx->target_node = target_node;
	msg_to_tx->msg_type = MSG_REQ_AVAIL;
	msg_to_tx->payload_size = 0;
	
	RFM_ShowMsg(msg_to_tx, false);
	RFM_SendMsg(msg_to_tx);
	
	for(uint8_t wait_for_response = 0; wait_for_response < ACK_RESPONSE_ATTEMPT; wait_for_response++)
	{
		if(RFM_Avail())
		{
			RFM_RcvMsg(&rcv_msg);
			RFM_ProcessRcvMsg(&rcv_msg);
			
			if(rcv_msg.target_node == this_node.addr && rcv_msg.source_node == target_node && rcv_msg.msg_type == MSG_ACK_AVAIL)
			{
				node_avail = true;
				Serial.print("\n\n\nI HAVE FOUND ");
				Serial.println(target_node, DEC);
				
				break;
			}
		}
		delay(ACK_RESPONSE_DLY);
	}
	
	
	return node_avail;
}

/*
FUNC:		RFM_BroadcastHello
INPUT:		void
OUTPUT:		void
PURPOSE:	Broadcast to all node within range a test message.
*/
void RFM_BroadcastHello(void)
{
	uint8_t data[] = "Hello world";
	
	if(this_node.init)
	{
		RFM69.send(data, sizeof(data));
		RFM69.waitPacketSent();
		
		Serial.println("Broadcast - 'Hello world'");
	}
	else
	{
		RFM_LED_Blink(6, LED_ERROR_DLY);
		
		Serial.println("Error - RFM69 not init successfully.");
	}	
}

/*
FUNC:		RFM_RcvBroadcast
INPUT:		void
OUTPUT:		void
PURPOSE:	Receives a broadcast test message from a node.
			Must be manually enabled or else msg parser will detect
			an incorrectly formed msg.
*/
void RFM_RcvBroadcast(void)
{
	uint8_t buff[RH_RF69_MAX_MESSAGE_LEN];
	uint8_t len = sizeof(buff);
	
	if(RFM69.recv(buff, &len))
	{
		buff[len] = 0x00;
		
		if(len != 0 && strstr((char *)buff, "Hello world"))
		{
			Serial.print("Receive broadcast - RSSI: ");
			Serial.print(RFM69.lastRssi(), DEC);
			Serial.print(" - ");
			Serial.println((char*)buff);
		}
	}
}

/*
FUNC:		RFM_ResetMsg
INPUT:		msg* msg_to_reset	- Node msg to reset.
OUTPUT:		void
PURPOSE:	Resets a node msg.
*/
void RFM_ResetMsg(msg* msg_to_reset)
{
	msg_to_reset->source_node = 0x00;
	msg_to_reset->target_node = 0x00;
	msg_to_reset->msg_type = MSG_NO_MSG;
	msg_to_reset->payload_size = 0x00;
	msg_to_reset->msg_rcvd_success = false;
	msg_to_reset->rssi = 0x00;
	
	for(int i = 0; i < PAYLOAD_BUFF_SIZE; i++)
	{
		msg_to_reset->payload_buff[i] = 0x00;
	}
}

//BOARD SUPPORT FUNCTIONS

/*
FUNC:		RFM_LED_Blink
INPUT:		int num_times - number of times to blink.
			int delay - amount of delay between blink.
OUTPUT:		void
PURPOSE:	Blinks the RFM69 on board LED.
*/
void RFM_LED_Blink(int num_times, int delay_ms)
{
	for(int i = 0; i < num_times; i++)
	{
		RFM_LED = !RFM_LED;
		digitalWrite(RFM_LED_PIN, RFM_LED);
		delay(delay_ms/2);
		
		RFM_LED = !RFM_LED;
		digitalWrite(RFM_LED_PIN, RFM_LED);
		delay(delay_ms/2);
	}
}

/*
FUNC:		RFM_LED_State
INPUT:		int num_times - number of times to blink.
			int delay - amount of delay between blink.
OUTPUT:		void
PURPOSE:	Blinks the RFM69 on board LED.
*/
void RFM_LED_State(bool led_state)
{
	digitalWrite(RFM_LED_PIN, led_state);
	RFM_LED = led_state;
}
