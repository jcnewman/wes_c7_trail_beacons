#ifndef _USER_H
#define _USER_H

#include <stdint.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>


//STATUS
#define STATUS_NO_STATUS			0x00
#define STATUS_GOOD					0x01
#define STATUS_DISTRESS				0x02
#define STATUS_REQ_FOOD				0x03
#define STATUS_REQ_MEDICAL			0x04
#define STATUS_REQ_EGRESS			0x05

#define STATUS_MIN_SELECTION		0x01
#define STATUS_MAX_SELECTION		0x05


#define BTN_PRESS_IDLE				0x00
#define BTN_PRESS_UP				0x01
#define BTN_PRESS_DOWN				0x02
#define BTN_PRESS_SELECT			0x03

#define BTN_DLY_THRESH				1000

#define BTN_PIN_A		9
#define BTN_PIN_B		6
#define BTN_PIN_C		5

#define OLED_ADDR		0x3C
#define OLED_WIDTH		128
#define OLED_HEIGHT		32
#define OLED_SHOW_DLY	150
#define OLED_START_DLY	1000

#define MENU_SEC_TIMEOUT		3
#define GPS_NOT_ACTIVE_DISP		1000


class User
{
	private:
	Adafruit_SSD1306 oled = Adafruit_SSD1306(OLED_WIDTH, OLED_HEIGHT, &Wire);
	unsigned long btn_A_dly;
	unsigned long btn_B_dly;
	unsigned long btn_C_dly;
	uint8_t node_addr;
	
	signed long int gps_lat;
	signed long int gps_lon;
	bool gps_acq;
	uint8_t gps_hh;
	uint8_t gps_mm;
	uint8_t gps_ss;
	uint8_t gps_month;
	uint8_t gps_day;
	uint8_t gps_year;
	
	bool btn_A_press;
	bool btn_B_press;
	bool btn_C_press;
	uint8_t last_ss;
	uint8_t last_menu_count;
	unsigned long last_milli_count;
	
	
	
	public:
	uint8_t menu_select;
	uint8_t stat;
		
	User(void);
	
	void InitDisplay(void);
	
	void Press_A(void);
	void Press_B(void);
	void Press_C(void);
	
	void Press(uint8_t btn_press);
	uint8_t GetStatus(void);
	
	void SetNodeID(uint8_t node_num);
	void UpdateGPS(bool acq, signed long int lat, signed long int lon,
		uint8_t hh, uint8_t mm, uint8_t ss,
		uint8_t month, uint8_t day, uint8_t year);
	void ShowDisplay(void);
	
};


extern User hiker;

#endif
