


#ifndef _RFM_H
#define _RFM_H

#include <RH_RF69.h>
#include <SPI.h>
#include "GPS.h"

#define PERFORM_SEARCH_NODE_OF_INTEREST_FIRST	0
#define TRANSMIT_PWR_LVL						20

//PIN DEFINES FOR RFM69 ON FEATHER M0
#define RFM_CS_PIN        						8
#define RFM_INT_PIN       						3
#define RFM_RST_PIN       						4
#define RFM_LED_PIN					    		13

//RADIO CONSTANTS
#define RFM_RESET_PIN_DELAY						10
#define PAYLOAD_BUFF_SIZE         				18
#define MSG_HEADER_SIZE							5
#define MSG_STATUS_SIZE							10
#define NODES_ON_PATH_SIZE						PAYLOAD_BUFF_SIZE-1
#define RFM_RCV_ACK_THRESH						-90
#define RFM_RCV_MSG_THRESH						-150


//MESSAGE STRUCTURE
#define SENTINEL                      			'@'
#define HOST_ADDR								1

//MESSAGE TYPES
#define MSG_NO_MSG						        0x00
#define MSG_REQ_AVAIL					        0x01
#define MSG_ACK_AVAIL					        0x02
#define MSG_REQ_STATUS_NODE				    	0x03
#define MSG_ACK_STATUS_NODE_NOT_FOUND			0x04
#define MSG_ACK_STATUS_NODE_FOUND		 	 	0x05
#define MSG_INVALID								0xFF

/*
NO_MSG
	Default state when no message is available to tx or rx.
REQ_AVAIL
	Transmit a REQ_AVAIL to detect if a TARGET_NODE is within range.
	If the TARGET_NODE is within range then it will respond with an
	ACK_AVAIL back at the SOURCE_NODE, which is THIS_NODE.
ACK_AVAIL
	The TARGET_NODE will receive a REQ_AVAIL, and if the TARGET_NODE
	address matches THIS_NODE_ADDR then THIS_NODE will respond with
	an ACK_AVAIL back at the SOURCE_NODE.
REQ_STATUS_NODE
	A SOURCE_NODE will request the status from a NODE_OF_INTEREST.
	If the TARGET_NODE is the NODE_OF_INTEREST, then the TARGET_NODE
	will respond with an ACK_STATUS_NODE_FOUND. If the TARGET_NODE
	is not the NODE_OF_INTEREST, then the TARGET_NODE will propagate the
	the REQ_STATUS_NODE to another neighboring node until no nodes are available
	to propagate the message to. Then the TARGET_NODE will respond back with
	ACK_STATUS_NODE_NOT_FOUND.
ACK_STATUS_NODE_NOT_FOUND
	Acknowledge message back to the SOURCE_NODE that the NODE_OF_INTEREST
	is not within the list of neighboring nodes to the TARGET_NODE.
ACK_STATUS_NODE_FOUND
	Acknowledge message that THIS_NODE is the NODE_OF_INTEREST.
*/

//STATUS
#define STATUS_NO_STATUS			0x00
#define STATUS_GOOD					0x01
#define STATUS_DISTRESS				0x02
#define STATUS_YES					0x03
#define STATUS_NO					0x04
#define STATUS_REQ_FOOD				0x05
#define STATUS_REQ_MEDICAL			0x06
#define STATUS_REQ_EGRESS			0x07

//LED STATES
#define LED_BLINK_DLY				500
#define LED_ERROR_DLY				250

#define ACK_RESPONSE_ATTEMPT				5
#define ACK_RESPONSE_DLY					10



typedef struct
{
	bool init;
	uint8_t addr;
	float freq;
	
	uint8_t rx_msg;
	uint8_t tx_msg;
	
	signed long int lat;
	signed long int lon;
	
	uint8_t stat;
}node;

typedef struct
{
  uint8_t source_node;
  uint8_t target_node;
  uint8_t msg_type;
  uint8_t payload_size;
  uint8_t payload_buff[PAYLOAD_BUFF_SIZE];
  int rssi;
  bool msg_rcvd_success;
}msg;

typedef struct
{
	uint8_t node_of_interest;
	uint8_t num_nodes_on_path;
	uint8_t nodes_on_path[NODES_ON_PATH_SIZE];
	uint8_t num_failed_nodes;
	uint8_t failed_nodes_list[NODES_ON_PATH_SIZE];
	uint8_t check_target_node;
}route;
	

extern msg tx_msg;
extern msg rcv_msg;
extern route node_route;


extern bool RFM_LED;

//this_node is the info pertaining to this node.
//When constructing a data packet, this node
//is also the SOURCE_NODE.
extern node this_node;

void RFM_Init(uint8_t this_node_addr, float freq);
void RFM_Reset(void);

bool RFM_Avail(void);
void RFM_SendMsg(msg* msg_to_tx);
void RFM_RcvMsg(msg* msg_to_rcv);
void RFM_ProcessRcvMsg(msg* rcv_msg);

void RFM_ShowMsg(msg* show_msg, bool rcv);
bool RFM_IsNodeAvail(msg* msg_to_tx, uint8_t target_node);

void RFM_BroadcastHello(void);
void RFM_RcvBroadcast(void);

void RFM_ResetMsg(msg* msg_to_reset);

void RFM_LED_Blink(int num_times, int delay_ms);
void RFM_LED_State(bool led_state);


#endif
