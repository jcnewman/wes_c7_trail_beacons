#include "GPS.h"
#include <RadioHead.h>		//I use this to get Serial Port access


GPS node_gps;

/*
FUNC:		Init
INPUT:		void
OUTPUT:		NA
PURPOSE:	GPS constructor.
*/
GPS::GPS(void)
{
	
}

/*
FUNC:		Init
INPUT:		void
OUTPUT:		void
PURPOSE:	Initializes the GPS state of object.
*/
void GPS::Init(void)
{
	strncpy(_GPS_strmatch, GPS_STRMATCH, GPS_STRMATCH_SIZE);
	
	ResetGPS();
	//Serial.println("GPS init complete");
}



//Sample string [70 characters]
//$GPRMC,020447.000,A,3242.2364,N,11714.2916,W,0.28,137.91,210419,,,D*7E

//$GPRMC,202
//613.000,A,
//3242.2413,
//N,11714.29
//78,W,0.02,
//229.82,270
//419,,,D*70

//$GPRMC		msg type
//020447.000	UTC time 02:04:47 UTC
//A				Sat acquired
//3242.2364		Lat: 32deg 42.2364'
//N				North
//11714.2916	Lon: 117deg 14.2916'
//W				West
//0.28			Speed over ground in knots
//137.91		Track angle in degrees (true)
//210419		Date: 04/21/2019
//D*7E			Checksum

/*
FUNC:		ProcessByte
INPUT:		uint8_t rx_byte		-Incoming GPS byte to process.
OUTPUT:		void
PURPOSE:	Process byte by byte of the NMEA0183 string.
			Looks for GPRMC string.
*/
void GPS::ProcessByte(uint8_t rx_byte)
{
	int start_ind = -1;
	char str[GPS_STRMATCH_SIZE];
	signed long int var = 0;
	memset(str, '\0', GPS_STRMATCH_SIZE);
	
	//Check if the buffer has not started yet and if 1st char is $
	if(rx_byte == '$')
	{
		ResetBuff();
		
		_GPS_buffind++;
		_GPS_buff[_GPS_buffind] = rx_byte;
	}
	
	//You are still building your GPS packet
	if(_GPS_buffind >= 0 && _GPS_buffind < 70 && rx_byte != '$')
	{
		_GPS_buffind++;
		_GPS_buff[_GPS_buffind] = rx_byte;
		
		//Serial.print((char)rx_byte);
		
		if(rx_byte == ',')
		{
			_comma_pos[_comma_ind] = _GPS_buffind;
			_comma_ind++;
		}
	}
	
	//You have found what you think is a well formed GPS packet
	if(_GPS_buffind == 69)
	{
		//process string and look for ind.
		//|$GPRMC|,020447.000,A,3242.2364,N,11714.2916,W,0.28,137.91,210419,,,D*7E
		strncpy(str, &_GPS_buff[0], 6);
		
		start_ind = strcmp(str, _GPS_strmatch);
		
		//You found a well formed packet
		if(start_ind == 0)
		{
			//$GPRMC,|020447|.000,A,3242.2364,N,11714.2916,W,0.28,137.91,210419,,,D*7E
			memset(str, '\0', GPS_STRMATCH_SIZE);
			strncpy(str, &_GPS_buff[_comma_pos[0] + 1], 6);
			
			var = strtol(str, NULL, 10);
			
			hh = var/10000;
			mm = (var/100)%100;
			ss = var%100;
			
			//$GPRMC,020447.000,|A|,3242.2364,N,11714.2916,W,0.28,137.91,210419,,,D*7E
			
			memset(str, '\0', GPS_STRMATCH_SIZE);
			str[0] = _GPS_buff[_comma_pos[1] + 1];
			
			if(str[0] == 'A')
			{
				acq = true;
				
				//$GPRMC,020447.000,A,|3242.2364|,N,11714.2916,W,0.28,137.91,210419,,,D*7E
				memset(str, '\0', GPS_STRMATCH_SIZE);
				strncpy(str, &_GPS_buff[_comma_pos[2] + 1], 9);
				lat = (atof(str)*10000);
				
				//$GPRMC,020447.000,A,3242.2364,|N|,11714.2916,W,0.28,137.91,210419,,,D*7E
				if(_GPS_buff[_comma_pos[3] + 1] == 'S')
				{
					lat = -1*lat;
				}
				
				//$GPRMC,020447.000,A,3242.2364,N,|11714.2916|,W,0.28,137.91,210419,,,D*7E
				memset(str, '\0', GPS_STRMATCH_SIZE);
				strncpy(str, &_GPS_buff[_comma_pos[4] + 1], 10);
				lon = (atof(str)*10000);
				
				//$GPRMC,020447.000,A,3242.2364,N,11714.2916,|W|,0.28,137.91,210419,,,D*7E
				if(_GPS_buff[_comma_pos[5] + 1] == 'W')
				{
					lon = -1*lon;
				}
				
				//$GPRMC,020447.000,A,3242.2364,N,11714.2916,W,0.28,137.91,|210419|,,,D*7E
				
				memset(str, '\0', GPS_STRMATCH_SIZE);
				strncpy(str, &_GPS_buff[_comma_pos[8] + 1], 6);
			
				var = strtol(str, NULL, 10);
				day = var/10000;
				month = (var/100)%100;
				year = var%100;
				
			}
			else
			{
				acq = false;
				
				lat = 0;
				lon = 0;
				hh = 0;
				mm = 0;
				ss = 0;
				month = 0;
				day = 0;
				year = 0;
			}
			
			/*
			Serial.println("\nGPS INFO:");
			Serial.print(month);
			Serial.print('/');
			Serial.print(day);
			Serial.print('/');
			Serial.print(year);
			Serial.print('\t');
			Serial.print(hh);
			Serial.print(':');
			Serial.print(mm);
			Serial.print(':');
			Serial.println(ss);
			Serial.print('[');
			Serial.print(lat);
			Serial.print(' ');
			Serial.print(lon);
			Serial.println("]\n");
			*/
			
		}
		
		//Reset the buffer regardless if you find a well formed packet
		ResetBuff();
		
		for(int i = 0; i < 12; i++)
		{
			_comma_pos[i] = 0x00;
			_comma_ind = 0;
		}
		
	}
	
	//If it falls out of boundary for any reason
	if(_GPS_buffind > 69)
	{
		ResetBuff();
		
	}
	
}

/*
FUNC:		ResetBuff
INPUT:		void
OUTPUT:		void
PURPOSE:	Resets the buffer.
*/
void GPS::ResetBuff(void)
{
	_GPS_buffind = -1;
	_comma_ind = 0;
	
	memset(_GPS_strmatch, 0x00, GPS_STRMATCH_SIZE);
	memset(_GPS_buff, 0x00, GPS_BUFF_SIZE);
	memset(_comma_pos, 0x00, 12);
	
	strcpy(_GPS_strmatch, GPS_STRMATCH);
}

/*
FUNC:		ResetGPS
INPUT:		void
OUTPUT:		void
PURPOSE:	Reset the GPS states.
*/
void GPS::ResetGPS(void)
{
	lat = 0;
	lon = 0;
	
	acq = false;
	
	hh = 0;
	mm = 0;
	ss = 0;
	
	month = 0;
	day = 0;
	year = 0;
	
	ResetBuff();
}

/*
FUNC:		ShowTimeStamp
INPUT:		void
OUTPUT:		void
PURPOSE:	Display human readable GPS timestamp.
*/
void GPS::ShowTimeStamp(void)
{
	Serial.print(month);
	Serial.print("/");
	Serial.print(day);
	Serial.print("/");
	Serial.print(year);
	Serial.print("\t");
	Serial.print(hh);
	Serial.print(":");
	Serial.print(mm);
	Serial.print(":");
	Serial.print(ss);
}

/*
FUNC:		ShowLatLon
INPUT:		void
OUTPUT:		void
PURPOSE:	Display human readable lat/lon coordinates.
*/
void GPS::ShowLatLon(void)
{
	int lat_deg = (lat)/1000000;
	int lon_deg = (lon)/1000000;
	
	float lat_dec = 0;
	float lon_dec = 0;
	
	if(lat < 0)
	{
		lat_dec = ((float)((-1*lat)%1000000))/10000;
	}
	else
	{
		lat_dec = ((float)(lat%1000000))/10000;
	}
	
	if(lon < 0)
	{
		lon_dec = ((float)(-1*lon%1000000))/10000;
	}
	else
	{
		lon_dec = ((float)(lon%1000000))/10000;
	}
	
	Serial.print("[");
	Serial.print(lat_deg);
	Serial.print(" ");
	Serial.print(lat_dec, 4);
	Serial.print(", ");
	Serial.print(lon_deg);
	Serial.print(" ");
	Serial.print(lon_dec, 4);
	Serial.print("]");
}