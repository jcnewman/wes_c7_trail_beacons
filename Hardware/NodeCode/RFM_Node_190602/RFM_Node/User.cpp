#include "User.h"
#include <stdint.h>
#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>


User hiker;

/*
FUNC:		User
INPUT:		void
OUTPUT:		NA
PURPOSE:	User constructor.
*/
User::User(void)
{
	//OLED Startup
	menu_select = STATUS_GOOD;
	stat = STATUS_GOOD;
	
  
	btn_A_press = false;
	btn_B_press = false;
	btn_C_press = false;
	
	btn_A_dly = millis();
	btn_B_dly = millis();
	btn_C_dly = millis();
	
	last_milli_count = millis();
	
	last_ss = 0;
	last_menu_count = 0;
	
}

/*
FUNC:		InitDisplay
INPUT:		void
OUTPUT:		void
PURPOSE:	Initializes the display.
*/
void User::InitDisplay(void)
{
	oled.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
	oled.display();
	delay(OLED_START_DLY);
	
	oled.clearDisplay();
	oled.display();
	
	oled.setTextSize(1);
	oled.setTextColor(WHITE);
	oled.setCursor(0,0);
}

/*
FUNC:		Press_A
INPUT:		void
OUTPUT:		void
PURPOSE:	Toggles a flag when BtnA is pressed.
			Corresponds to UP on the menu.
*/
void User::Press_A(void)
{
	if(millis() - btn_A_dly > BTN_DLY_THRESH)
	{
		menu_select = menu_select + 1;
		if(menu_select > STATUS_MAX_SELECTION)
		{
			menu_select = STATUS_MIN_SELECTION;
		}
		btn_A_press = true;
	}
	
	btn_A_dly = millis();	
}

/*
FUNC:		Press_B
INPUT:		void
OUTPUT:		void
PURPOSE:	Toggles a flag when BtnB is pressed.
			Corresponds to SELECT on the menu.
*/
void User::Press_B(void)
{
	if(millis() - btn_B_dly > BTN_DLY_THRESH)
	{
		stat = menu_select;
		btn_B_press = true;
	}
	
	btn_B_dly = millis();	
}

/*
FUNC:		Press_C
INPUT:		void
OUTPUT:		void
PURPOSE:	Toggles a flag when BtnC is pressed.
			Corresponds to DOWN on the menu.
*/
void User::Press_C(void)
{
	if(millis() - btn_C_dly > BTN_DLY_THRESH)
	{
		if(menu_select  == STATUS_MIN_SELECTION)
		{
			menu_select = STATUS_MAX_SELECTION;
		}
		else
		{
			menu_select = menu_select - 1;
		}
		btn_C_press = true;
	}
	
	btn_C_dly = millis();	
}

/*
FUNC:		GetStatus
INPUT:		void
OUTPUT:		uint8_t
PURPOSE:	Returns the status of the user.
*/
uint8_t User::GetStatus(void)
{
	return stat;
}

/*
FUNC:		SetNodeID
INPUT:		void
OUTPUT:		void
PURPOSE:	Sets the node address of the user.
*/
void User::SetNodeID(uint8_t node_num)
{
	node_addr = node_num;
}

/*
FUNC:		UpdateGPS
INPUT:		bool acq			- Acquired state for the user. True for GPS acquired, false otherise.
			signed long int lat	- latitude, degree decimal.
			signed long int lon	- longitude, degree decimal.
OUTPUT:		void
PURPOSE:	Updates User object with location status.
*/
void User::UpdateGPS(bool acq, signed long int lat, signed long int lon,
	uint8_t hh, uint8_t mm, uint8_t ss,
	uint8_t month, uint8_t day, uint8_t year)
{
	gps_lat = lat;
	gps_lon = lon;
	
	gps_hh = hh;
	gps_mm = mm;
	gps_ss = ss;
	
	gps_month = month;
	gps_day = day;
	gps_year = year;
	
	gps_acq = acq;
}

/*
FUNC:		ShowDisplay
INPUT:		void
OUTPUT:		void
PURPOSE:	Refreshes OLED display.
*/
void User::ShowDisplay(void)
{
	signed long int factor = 1;
	signed long int m = 1;
	signed long int val = 0;
	signed long int lat = gps_lat;
	signed long int lon = gps_lon;
	
	bool display_oled = false;
	
	//Determine when to display the OLED
	//This will require OLED_SHOW_DLY time to fully buffer screen
	if(last_ss != gps_ss)
	{
		display_oled = true;
		last_ss = gps_ss;
	}
	else if(btn_A_press || btn_C_press)
	{
		display_oled = true;
		last_menu_count = 0;
	}
	else if(btn_B_press)
	{
		display_oled = true;
		last_menu_count = MENU_SEC_TIMEOUT + 1;
	}
	else if(!gps_acq && (millis() - last_milli_count > GPS_NOT_ACTIVE_DISP))
	{
		display_oled = true;
		last_milli_count = millis();
		Serial.println(millis() - last_milli_count);
	}
	else
	{
		display_oled = false;
	}
	
	
	if(display_oled)
	{
	
		oled.clearDisplay();
		oled.setCursor(0, 0);
		
		
		//LINE 1: NODE ID | STATUS
		if(node_addr == 0)
		{
			oled.print("???");
		}
		else if(node_addr == 1)
		{
			oled.print("HOST");
		}
		else
		{
			oled.print("#");
			oled.print(node_addr);
		}
		
		oled.print(" | ");
		
		if(btn_A_press || btn_C_press || last_menu_count <= MENU_SEC_TIMEOUT)
		{

			switch(menu_select)
			{
				case STATUS_NO_STATUS:
					oled.print("NO STATUS");
					break;
				
				case STATUS_GOOD:
					oled.print("GOOD");
					break;
				
				case STATUS_DISTRESS:
					oled.print("DISTRESS");
					break;
				
				case STATUS_REQ_FOOD:
					oled.print("REQ_FOOD");
					break;
				
				case STATUS_REQ_MEDICAL:
					oled.print("REQ_MED");
					break;
				
				case STATUS_REQ_EGRESS:
					oled.print("REQ_EGRESS");
					break;
				
				default:
					oled.print("???");
					break;
			
			}
			
			last_menu_count++;
		}
		
		else
		{
			oled.print("[");
			switch(stat)
			{
				case STATUS_NO_STATUS:
					oled.print("NO STATUS");
					break;
				
				case STATUS_GOOD:
					oled.print("GOOD");
					break;
				
				case STATUS_DISTRESS:
					oled.print("DISTRESS");
					break;
				
				case STATUS_REQ_FOOD:
					oled.print("REQ_FOOD");
					break;
				
				case STATUS_REQ_MEDICAL:
					oled.print("REQ_MED");
					break;
				
				case STATUS_REQ_EGRESS:
					oled.print("REQ_EGRESS");
					break;
				
				default:
					oled.print("???");
					break;
			
			}
			oled.print("]");			
		}
		
		
		oled.println();

		//LINE 3: DATE/TIME
		//LINE 4: GPS LAT LON
		if(gps_acq)
		{	
			
			oled.print(gps_month);
			oled.print("/");
			oled.print(gps_day);
			oled.print("/");
			oled.print(gps_year);
			
			oled.print(" ");
			oled.print(gps_hh);
			oled.print(":");
			oled.print(gps_mm);
			oled.print(":");
			oled.print(gps_ss);
			
			oled.println();
			
			
			oled.print("LAT: ");
			if(lon < 0)
			{
				lon = -1*lon;
				oled.print("-");
			}
			else
			{
				oled.print(" ");
			}
			
			factor = 100000000;
			for(int i = 0; i < 9; i++)
			{
				val = (lon/factor) % 10;
				oled.print(val);
				if(i == 2)
				{
					oled.print(" ");
				}
				if(i == 4)
				{
					oled.print(".");
				}
				
				factor = factor/10;
			}
			
			oled.println();
			oled.print("LON: ");
			
			if(lat < 0)
			{
				lat = -1*lat;
				oled.print("-");
			}
			else
			{
				oled.print(" ");
			}
			
			
			factor = 100000000;
			for(int i = 0; i < 9; i++)
			{
				val = (lat/factor) % 10;
				oled.print(val);
				if(i == 2)
				{
					oled.print(" ");
				}
				if(i == 4)
				{
					oled.print(".");
				}
				
				factor = factor/10;
			}	
			
		}
		else
		{
			oled.println("GPS NOT ACTIVE");
		}
		
		btn_A_press = false;
		btn_B_press = false;
		btn_C_press = false;
		
		
		oled.display();
		//delay(OLED_SHOW_DLY);
	}
	
}

