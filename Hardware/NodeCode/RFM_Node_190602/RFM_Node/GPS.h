#ifndef _GPS_H
#define _GPS_H

#include<stdint.h>
#include<stdio.h>
#include<string.h>


#define GPS_BUFF_SIZE		128
#define GPS_STRMATCH_SIZE	7
#define GPS_STRMATCH		"$GPRMC"




class GPS
{
	private:
	int _GPS_buffind;
	char _GPS_buff[GPS_BUFF_SIZE];
	char _GPS_strmatch[GPS_STRMATCH_SIZE];
	uint8_t _comma_pos[12];
	int _comma_ind;
	
	//HardwareSerial* _GPS_serial;
	
	public:
	
	signed long int lat;
	signed long int lon;
	
	bool acq;
	
	uint8_t hh;
	uint8_t mm;
	uint8_t ss;
	
	uint8_t month;
	uint8_t day;
	uint8_t year;
	
	
	//GPS(HardwareSerial* GPS_serial);
	GPS(void);
	
	//const char* Get_TimeStamp(void);
	
	void Init(void);
	void ProcessByte(uint8_t rx_byte);
	void ResetBuff(void);
	void ResetGPS(void);
	void ShowTimeStamp(void);
	void ShowLatLon(void);
	
};

extern GPS node_gps;


#endif
