/*
Trail Beacon
Revision
190427 - 	Basic node architecture working.
			Can find a node of interest through the network.
			
190504 - 	190501 copy not working. No receive serial port.
			Use 190502 instead of 190501.
			REQ_STATUS_NODE will first search for NODE_OF_INTEREST
			then find nearest neighbor to start the node search process.
			Lower the threshold on the ACK and MSG threshold.

190524 -	Integrated the User IO interface. This includes:
				1. Button A, B, C for menu selection.
				2. OLED display of selection and GPS time/lat/lon.
				3. Status field is enabled and feeding host node actual status results.
				
190602 -	Comments because I won't remember any of this in the future. You're welcome reader...

*/


#include <SPI.h>

#include "RFM.h"
#include "GPS.h"
#include "User.h"

//NODE SETUP
#define NODE_ADDR   			7
#define NODE_FREQ   			915.00


//BOARD SETUP
#define USER_SERIAL_BAUD    	115200
#define GPS_SERIAL_BAUD     	9600

#define USER_CMD_BROADCAST			'b'
#define USER_CMD_BLINKLED			'l'
#define USER_CMD_REQ_AVAIL			'a'
#define USER_CMD_NEIGHBOR_NODES		'n'
#define USER_CMD_IDENTITY			'i'
#define USER_CMD_REQ_NODE_STATUS	's'
#define USER_CMD_HELP				'h'


void setup()
{
	//Start user serial
	Serial.begin(USER_SERIAL_BAUD);
	Serial1.begin(GPS_SERIAL_BAUD);
	
	delay(1000);
	
	node_gps.Init();

	RFM_Init(NODE_ADDR, NODE_FREQ);
	
	pinMode(BTN_PIN_A, INPUT_PULLUP);
	pinMode(BTN_PIN_B, INPUT_PULLUP);
	pinMode(BTN_PIN_C, INPUT_PULLUP);
	
	attachInterrupt(BTN_PIN_A, Btn_A_Int, CHANGE);
	attachInterrupt(BTN_PIN_B, Btn_B_Int, CHANGE);
	attachInterrupt(BTN_PIN_C, Btn_C_Int, CHANGE);
	
	hiker.InitDisplay();
	hiker.SetNodeID(this_node.addr);
	
	Serial.print("USER_CMD_BROADCAST\t\t");
	Serial.println(USER_CMD_BROADCAST);
	Serial.print("USER_CMD_BLINKLED\t\t");
	Serial.println(USER_CMD_BLINKLED);
	Serial.print("USER_CMD_REQ_AVAIL\t\t");
	Serial.println(USER_CMD_REQ_AVAIL);
	Serial.print("USER_CMD_NEIGBOR_NODES\t\t");
	Serial.println(USER_CMD_NEIGHBOR_NODES);
	Serial.print("USER_CMD_IDENTITY\t\t");
	Serial.println(USER_CMD_IDENTITY);
	Serial.print("USER_CMD_REQ_NODE_STATUS\t");
	Serial.println(USER_CMD_REQ_NODE_STATUS);
	
}

void loop()
{
	uint8_t user_cmd = 0x00;
	uint8_t node_num = 0x00;
	uint8_t gps_byte = 0x00;
		
		
	while(Serial1.available())
	{
		gps_byte = Serial1.read();
		node_gps.ProcessByte(gps_byte);
	}
	
	//User enter command
	if(Serial.available())
	{
		user_cmd = Serial.read();
	}
	
	if((char)user_cmd == USER_CMD_HELP)
	{
		Serial.print("1. USER_CMD_BROADCAST\t\t");
		Serial.println(USER_CMD_BROADCAST);
		Serial.println("\tBroadcast a message to all nodes, not supported.");
		
		Serial.print("2. USER_CMD_BLINKLED\t\t");
		Serial.println(USER_CMD_BLINKLED);
		Serial.println("\tBlinks the LED of THIS_NODE.");
		
		Serial.print("3. USER_CMD_REQ_AVAIL\t\t");
		Serial.print(USER_CMD_REQ_AVAIL);
		Serial.println(" + NODE_NUM");
		Serial.println("\tChecks if NODE_NUM is within range.");
		
		Serial.print("4. USER_CMD_NEIGBOR_NODES\t\t");
		Serial.println(USER_CMD_NEIGHBOR_NODES);
		Serial.println("\tFinds all nodes within range.");
		
		Serial.print("5. USER_CMD_IDENTITY\t\t");
		Serial.println(USER_CMD_IDENTITY);
		Serial.println("\tChecks the info on THIS_NODE.");
		
		Serial.print("6. USER_CMD_REQ_NODE_STATUS\t");
		Serial.print(USER_CMD_REQ_NODE_STATUS);
		Serial.println(" + NODE_NUM");
		Serial.println("\tRequest the network to look for NODE_NUM");
		Serial.println("");
	}
	if((char)user_cmd == USER_CMD_BROADCAST)
	{
		RFM_BroadcastHello();
	}
	if((char)user_cmd == USER_CMD_BLINKLED)
	{
		RFM_LED_Blink(6, 100);
	}
	if((char)user_cmd == USER_CMD_REQ_AVAIL)
	{
		while(!Serial.available());
		{
		}
		node_num = Serial.read();
		node_num = node_num - 48;		//ascii to dec
		
		//Construct your transmit node message
		tx_msg.source_node = this_node.addr;
		tx_msg.target_node = node_num;
		tx_msg.msg_type = MSG_REQ_AVAIL;
		tx_msg.payload_size = 0;

		RFM_ShowMsg(&tx_msg, false);
		RFM_SendMsg(&tx_msg);
	}
	
	if((char)user_cmd == USER_CMD_NEIGHBOR_NODES)
	{
		uint8_t neighbor_node_addr = 1;
		uint8_t neighbor_node_list[NODES_ON_PATH_SIZE] = {0};
		int neighbor_node_rssi[NODES_ON_PATH_SIZE] = {0};
		uint8_t neighbor_node_ind = 0;
		
		bool node_found = false;
		
		Serial.println("Starting neighboring nodes search...");
		
		for(uint8_t nn = 1; nn < NODES_ON_PATH_SIZE; nn++)
		{
			node_found = false;
			
			//Exit condition
			if(Serial.available())
			{
				user_cmd = Serial.read();
				if(user_cmd == 'e')
				{
					Serial.println("User end node search.");
					Serial.println("");
					break;
				}
			}
			
			//skip its own address
			if(nn == this_node.addr)
			{
				nn++;
				Serial.println("Skip self node address.");
				Serial.println("");
			}
			
			if(RFM_IsNodeAvail(&tx_msg, nn))
			{
				neighbor_node_list[neighbor_node_ind] = rcv_msg.source_node;
				neighbor_node_rssi[neighbor_node_ind] = rcv_msg.rssi;
				neighbor_node_ind++;
			}
			
			Serial.println("\n");
			
		}
		
		Serial.println("Neighboring nodes are: ");
		
		for(int i = 0; i < neighbor_node_ind; i++)
		{
			Serial.print("node:\t");
			Serial.print(neighbor_node_list[i], DEC);
			Serial.print("\trssi: ");
			Serial.println(neighbor_node_rssi[i], DEC);
		}
		Serial.println("");
	}
	
	if(user_cmd == USER_CMD_IDENTITY)
	{
		RFM_LED_Blink(6, 100);
		
		Serial.println("-Node Info-");
		Serial.print("Node Addr: ");
		Serial.println(this_node.addr, DEC);
		
		if(this_node.init)
		{
			Serial.println("This node has been initialized.");
		}
		else
		{
			Serial.println("This node has not been initialized.");
		}
		
		Serial.print("Freq:\t");
		Serial.println(this_node.freq, DEC);
		
		//Serial.println("No GPS data avail");
		Serial.print("Time:\t");
		node_gps.ShowTimeStamp();
		Serial.println("");
		Serial.print("Loc:\t");
		node_gps.ShowLatLon();
		Serial.println();
		Serial.print("Status:\t");
		Serial.println(this_node.stat, DEC);
		Serial.println("");
	}
	
	if(user_cmd == USER_CMD_REQ_NODE_STATUS)
	{
		bool found_nn = false;
		//Get user input on which node to look for.
		while(!Serial.available());
		{
		}
		node_num = Serial.read();
		node_num = node_num - 48;		//ascii to dec
		node_route.node_of_interest = node_num;
		
		//Check if NODE_OF_INTEREST in range first.
		if(PERFORM_SEARCH_NODE_OF_INTEREST_FIRST)
		{
			if(RFM_IsNodeAvail(&tx_msg, node_route.node_of_interest))
			{
				//NODE_OF_INTEREST is within range.
				node_route.check_target_node = rcv_msg.source_node;
				node_route.num_nodes_on_path = 1;		//Host is only node on network so far.
				node_route.nodes_on_path[0] = this_node.addr;
				
				//Construct the message
				tx_msg.source_node = this_node.addr;
				tx_msg.target_node = node_route.check_target_node;
				tx_msg.msg_type = MSG_REQ_STATUS_NODE;
				tx_msg.payload_size = 2;
				tx_msg.payload_buff[0] = node_route.node_of_interest;
				
				for(int i = 0; i < node_route.num_nodes_on_path; i++)
				{
					tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
				}
			
				RFM_ShowMsg(&tx_msg, false);
				RFM_SendMsg(&tx_msg);
				
				found_nn = true;
			}

			
			if(found_nn)
			{
				Serial.print("Node ");
				Serial.print(this_node.addr, DEC);
				Serial.print(" is looking for node ");
				Serial.print(node_route.node_of_interest);
				Serial.print(" through node ");
				Serial.println(node_route.check_target_node, DEC);
				Serial.println("");
			}
			else
			{
				Serial.print("Node ");
				Serial.print(this_node.addr, DEC);
				Serial.print(" did not find ");
				Serial.print(node_num, DEC);
				Serial.println(" within range.");
				Serial.println("");
			}
		}
		
		//needs to find first node to hand the message to.
		//the RFM_ProcessRcvMsg() should handle reactive node msgs.
		if(!found_nn)
		{
			for(uint8_t nn = 1; nn < NODES_ON_PATH_SIZE; nn++)
			{
				//Exit condition
				if(Serial.available())
				{
					user_cmd = Serial.read();
					if(user_cmd == 'e')
					{
						Serial.println("User end node search.");
						Serial.println("");
						break;
					}
				}
				
				if(nn == this_node.addr)
				{
					nn++;
				}
				
				if(RFM_IsNodeAvail(&tx_msg, nn))
				{
					//Found a neighboring node to send REQ_STATUS_NODE
					node_route.node_of_interest = node_num;
					node_route.check_target_node = rcv_msg.source_node;
					node_route.num_nodes_on_path = 1;		//Host is only node on network so far.
					node_route.nodes_on_path[0] = this_node.addr;
					
					//Construct the message
					tx_msg.source_node = this_node.addr;
					tx_msg.target_node = node_route.check_target_node;
					tx_msg.msg_type = MSG_REQ_STATUS_NODE;
					tx_msg.payload_size = 2;
					tx_msg.payload_buff[0] = node_route.node_of_interest;
					
					for(int i = 0; i < node_route.num_nodes_on_path; i++)
					{
						tx_msg.payload_buff[i + 1] = node_route.nodes_on_path[i];
					}
				
					RFM_ShowMsg(&tx_msg, false);
					RFM_SendMsg(&tx_msg);
					
					nn = NODES_ON_PATH_SIZE;	//Breaking condition for outter for loop
					found_nn = true;			//Indicate we found a nn to hop message to.
					
					break;
				}
				
				if(found_nn)
				{
					Serial.print("Node ");
					Serial.print(this_node.addr, DEC);
					Serial.print(" is looking for node ");
					Serial.print(node_route.node_of_interest);
					Serial.print(" through node ");
					Serial.println(node_route.check_target_node, DEC);
					Serial.println("");
				}
				else
				{
					Serial.print("Node ");
					Serial.print(this_node.addr, DEC);
					Serial.print(" did not find ");
					Serial.print(nn, DEC);
					Serial.println(" within range.");
					Serial.println("");
				}
					
			}
		}
		
		if(!found_nn)
		{
			Serial.print("Node ");
			Serial.print(node_route.node_of_interest, DEC);
			Serial.println(" is not in the network");
			
			node_route.check_target_node = 1;
			node_route.num_nodes_on_path = 0;
		}
		
		
	}
	if(RFM_Avail())
	{
		//RFM_RcvBroadcast();
		RFM_RcvMsg(&rcv_msg);
		RFM_ProcessRcvMsg(&rcv_msg);
	}
	
	hiker.UpdateGPS(node_gps.acq, node_gps.lat, node_gps.lon, node_gps.hh, node_gps.mm, node_gps.ss, node_gps.month, node_gps.day, node_gps.year);
	hiker.ShowDisplay();
	this_node.stat = hiker.GetStatus();
	
}


void Btn_A_Int(void)
{
	if(digitalRead(BTN_PIN_A) == 0)
	{
		hiker.Press_A();
	}	
}

void Btn_B_Int(void)
{
	if(digitalRead(BTN_PIN_B) == 0)
	{
		hiker.Press_B();
	}
}

void Btn_C_Int(void)
{
	if(digitalRead(BTN_PIN_C) == 0)
	{
		hiker.Press_C();
	}
}
