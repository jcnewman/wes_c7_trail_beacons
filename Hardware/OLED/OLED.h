#include <Arduino.h>

//// OLED FeatherWing button pins
#define BUTTON_A  9
#define BUTTON_B  6
#define BUTTON_C  5

class OLED {
	private:
		//Flags
		bool initOLEDFlag;
		bool inMenu;
		int menuItem; // 0-food/water, 1-Physical Injury, 2-Lost, 3-Yogi Bear, 4-Snake Bite, 5-Good
		int nodeStatus;
		String message;
	public:
		OLED(void);
		void initOLED(void);
		void startMenu(void);
		void buttonCheck(void);
		void buttonA(void);
		void buttonB(void);
		void buttonC(void);
		void printMenu(void);
};
