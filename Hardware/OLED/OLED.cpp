#include "OLED.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

OLED::OLED() {
	initOLEDFlag = false;
	inMenu = false;
	menuItem = 5; // 0-food/water, 1-Physical Injury, 2-Lost, 3-Yogi Bear, 4-Snake Bite, 5-Good
	nodeStatus = 5;
	message = "";
}

void OLED::initOLED() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
 
 // Show image buffer on the display hardware.
 // Since the buffer is intialized with an Adafruit splashscreen
 // internally, this will display the splashscreen.
  display.display();
  delay(1000);
 
 // Clear the buffer.
  display.clearDisplay();
  display.display();

 //initialize button pins
  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);
 
  // initilize display settings
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  startMenu();
}

void OLED::startMenu() {
  display.clearDisplay();
  display.setCursor(0,0);
  display.println("A - Status Menu");
  display.setCursor(0,12);
  display.println("B - Scroll Menu");
  display.setCursor(0,24);
  display.println("C - Select Status");
  display.display();
  delay(150);
  initOLEDFlag = true;
  inMenu = false;
}

void OLED::buttonA(){
  delay(150);
  menuItem = 0;
  message = "I Need Food/Water";
  printMenu();
  inMenu = true;
}

void OLED::buttonB(){
  delay(150);
  if(inMenu == true && menuItem == 5){
    menuItem = 0;
    printMenu();
  }
  else if(inMenu == true){
    menuItem += 1;
    printMenu();
  }
}

void OLED::buttonC() {
  if (inMenu == true){
    nodeStatus = menuItem; 
  }
  delay(150);
}

void OLED::buttonCheck() {
  if(!digitalRead(BUTTON_A)){
    if(this->inMenu == false){
    buttonA();
    }
    else{initOLED();}
  }
  if(!digitalRead(BUTTON_B)){
    buttonB();
  }
  if(!digitalRead(BUTTON_C)){
    buttonC();
    display.setCursor(0,24);
    display.println(this->nodeStatus);
    display.display();
  }
}

void OLED::printMenu(){
  // Standard menu change
  /*display.clearDisplay();
  display.setCursor(0,0);
  display.println("Status Option:");
  display.display();
  display.setCursor(0,15);
  
  switch (menuItem) 
   { 
       case 0: display.println("I Need Food/Water"); 
               break; 
       case 1: display.println("I'm Injured"); 
               break; 
       case 2: display.println("I'm Lost");
               break; 
       case 3: display.println("Wild Animal Nearby");
               break;
       case 4: display.println("Snike Bite");
               break;
       case 5: display.println("I'm Okay");
               break;
       default: display.println("Error: Undefined \n'menuItem' value"); 
                break;                    
   }
   display.display(); 
   */
   //Scroll option to the left
   for (signed int a=0; a > (((signed int)message.length()) * -8); a-=8) {
          display.clearDisplay();
          display.setCursor(0,0);
          display.println("Status option:");
          display.setCursor(a,16);
          display.print(message);
          display.display();
    }
	//Update to new option
	switch (menuItem) 
   { 
       case 0: message = "I Need Food/Water"; 
               break; 
       case 1: message = "I'm Injured"; 
               break; 
       case 2: message = "I'm Lost";
               break; 
       case 3: message = "Wild Animal Nearby";
               break;
       case 4: message = "Snike Bite";
               break;
       case 5: message = "I'm Okay";
               break;
       default: message = "Error: Undefined \n'menuItem' value"; 
                break;                    
   }
	//Scroll new option onto the screen
	for (signed int a=(((signed int)message.length()) * -8); a <= 0; a+=8) {
          display.clearDisplay();
          display.setCursor(0,0);
          display.println("Status option:");
          display.setCursor(a,16);
          display.print(message);
          display.display();
    }
	//Last screen refresh so option is fully on screen
	display.clearDisplay();
	display.setCursor(0,0);
	display.println("Status option:");
	display.setCursor(0,16);
	display.print(message);
	display.display();
}
