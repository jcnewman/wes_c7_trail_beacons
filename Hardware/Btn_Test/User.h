#ifndef _USER_H
#define _USER_H

#include <stdint.h>

//STATUS
#define STATUS_NO_STATUS			0x00
#define STATUS_GOOD					0x01
#define STATUS_DISTRESS				0x02
#define STATUS_REQ_FOOD				0x03
#define STATUS_REQ_MEDICAL			0x04
#define STATUS_REQ_EGRESS			0x05

#define STATUS_MIN_SELECTION		0x00
#define STATUS_MAX_SELECTION		0x05


#define BTN_PRESS_IDLE				0x00
#define BTN_PRESS_UP				0x01
#define BTN_PRESS_DOWN				0x02
#define BTN_PRESS_SELECT			0x03

#define BTN_DLY_THRESH				1000


class User
{
	private:
	unsigned long btn_A_dly;
	unsigned long btn_B_dly;
	unsigned long btn_C_dly;
	public:
	
	uint8_t menu_select;
	uint8_t stat;
		
	User(void);
	
	void Press_A(void);
	void Press_B(void);
	void Press_C(void);
	
	void Press(uint8_t btn_press);
	uint8_t GetStatus(void);
};


extern User hiker;

#endif
