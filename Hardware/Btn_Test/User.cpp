#include "User.h"
#include <stdint.h>
#include <Arduino.h>

User hiker;

User::User(void)
{
	menu_select = 0x00;
	stat = 0x00;
	
	btn_A_dly = millis();
	btn_B_dly = millis();
	btn_C_dly = millis();
}

void User::Press_A(void)
{
	if(millis() - btn_A_dly > BTN_DLY_THRESH)
	{
		menu_select = menu_select + 1;
		if(menu_select > STATUS_MAX_SELECTION)
		{
			menu_select = STATUS_MIN_SELECTION;
		}
	}
	
	btn_A_dly = millis();	
}

void User::Press_B(void)
{
	if(millis() - btn_B_dly > BTN_DLY_THRESH)
	{
		stat = menu_select;
	}
	
	btn_B_dly = millis();	
}

void User::Press_C(void)
{
	if(millis() - btn_C_dly > BTN_DLY_THRESH)
	{
		if(menu_select  == STATUS_MIN_SELECTION)
		{
			menu_select = STATUS_MAX_SELECTION;
		}
		else
		{
			menu_select = menu_select - 1;
		}
	}
	
	btn_C_dly = millis();	
}


uint8_t User::GetStatus(void)
{
	return stat;
}
