
#include "User.h"

#define BTN_PIN_A		9
#define BTN_PIN_B		6
#define BTN_PIN_C		5
#define LED_PIN			13

bool LED;

int btn_A_count;
int btn_B_count;
int btn_C_count;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(BTN_PIN_A, INPUT_PULLUP);
  pinMode(BTN_PIN_B, INPUT_PULLUP);
  pinMode(BTN_PIN_C, INPUT_PULLUP);
  
  attachInterrupt(BTN_PIN_A, Btn_A_Int, CHANGE);
  attachInterrupt(BTN_PIN_B, Btn_B_Int, CHANGE);
  attachInterrupt(BTN_PIN_C, Btn_C_Int, CHANGE);
  
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, 0);
  
  LED = 0;
  btn_A_count = 0;
  btn_B_count = 0;
  btn_C_count = 0;
  
  
  for(int i = 0; i < 3; i++)
  {
	  
	  digitalWrite(LED_PIN, 1);
	  delay(250);
	  digitalWrite(LED_PIN, 0);
	  delay(250);
  }
  

}

void loop()
{
	if(millis()%1000 == 0)
	{
		Serial.print("Hiker status:\t");
		Serial.println(hiker.stat, DEC);
		Serial.print("Hiker menu:\t");
		Serial.println(hiker.menu_select, DEC);
		
		Serial.println();
	}
	
}

void Btn_A_Int(void)
{
	if(digitalRead(BTN_PIN_A) == 0)
	{
		hiker.Press_A();
	}	
}

void Btn_B_Int(void)
{
	if(digitalRead(BTN_PIN_B) == 0)
	{
		hiker.Press_B();
	}
}

void Btn_C_Int(void)
{
	if(digitalRead(BTN_PIN_C) == 0)
	{
		hiker.Press_C();
	}
}
