#include "SDLog.h"
#include <SPI.h>
#include <SD.h>

File myFile;

//Instantiate log with default name
SDLog::SDLog() {
	log = "DEFAULT.LOG";
}

//Instantiate log with custom name
//Use to avoid deleting log every session
SDLog::SDLog(char* logName) {
	log = logName;
}

//Initialize SD card and log file
//--WARNING--
//Will delete current log
void SDLog::init() {
	digitalWrite(8, HIGH);
	
	//Initialize SD card
	if (!SD.begin(10)) {
		while (1);
	}
	//Deletle current log file if it exists
	if (SD.exists(log)) {
		SD.remove(log);
	}
	//Create log file to write to
	myFile = SD.open(log, FILE_WRITE);
	if (myFile) {
		myFile.println("Start log");
		myFile.close();
	}
	digitalWrite(8, LOW);
}

//Append string to log on current line
void SDLog::write(char* str) {
  myFile = SD.open(log, FILE_WRITE);
  if (myFile) {
    myFile.print(str);
    myFile.close();
  }
  digitalWrite(8, LOW);
}

void SDLog::write(const char* str) {
  myFile = SD.open(log, FILE_WRITE);
  if (myFile) {
    myFile.print(str);
    myFile.close();
  }
  digitalWrite(8, LOW);
}

void SDLog::write(String str) {
  myFile = SD.open(log, FILE_WRITE);
  if (myFile) {
    myFile.println(str);
    myFile.close();
  }
  digitalWrite(8, LOW);
}
//Append line to current log file with newline character
void SDLog::writeln(char* str) {
	myFile = SD.open(log, FILE_WRITE);
	if (myFile) {
		myFile.println(str);
		myFile.close();
	}
	digitalWrite(8, LOW);
}

void SDLog::writeln(const char* str) {
  myFile = SD.open(log, FILE_WRITE);
  if (myFile) {
    myFile.println(str);
    myFile.close();
  }
  digitalWrite(8, LOW);
}

void SDLog::writeln(String str) {
  myFile = SD.open(log, FILE_WRITE);
  if (myFile) {
    myFile.println(str);
    myFile.close();
  }
  digitalWrite(8, LOW);
}

//Only works for reading config file with ID number (2 digits)
void SDLog::readln(int* buf, char* logName) {
	digitalWrite(8, HIGH);
	File dataFile = SD.open(logName, FILE_READ);
	/*if (dataFile) {
		uint32_t size = dataFile.size();
		*buf = (int *)malloc(size);
    while (dataFile.available()) {
      **buf = dataFile.read();
	  *buf++;
    }*/
  if (dataFile) {
    while (dataFile.available()) {
      *buf = dataFile.read();
      buf++;
    }
    dataFile.close();
	//*buf = dataFile.read();
	//*buf[size] = '\0';
	//dataFile.close();
	}
	digitalWrite(8, LOW);
}
