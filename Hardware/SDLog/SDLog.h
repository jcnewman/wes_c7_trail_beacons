#include <Arduino.h>

/*
 * Example:
 * 
 * // Declaration 
 * SDLog logger = SDLog(); OR SDLog("LogName");
 * 
 * // Initialize in setup method
 * logger.init();
 * 
 * // Write to log 
 * logger.write("Latitude - ");
 * // Printing variables with String library (only supports one at a time it seems)
 * logger.writeln(String(lat));
 */

class SDLog {
	private:
	
	public:
	char* log;
	SDLog(void);
	SDLog(char* logName);
	void init(void);
  void write(char* str);
  void write(const char* str);
  void write(String str);
	void writeln(char* str);
  void writeln(const char* str);
  void writeln(String str);
	void readln(int* buf, char* fileName);
};
